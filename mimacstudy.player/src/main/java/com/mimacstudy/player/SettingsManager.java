package com.mimacstudy.player;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Settings 관리자
 * @author kimsanghwan
 * @since 2015.11.24
 */
public class SettingsManager {

    private final static String KEY_PUSH_KEY = "push-key";  // push key
    private final static String KEY_OLD_PUSH_KEY = "old-push-key";  // old push Key
    private final static String KEY_USER_ID = "user-id";    // 사용자 id
    private final static String KEY_REGISTRATION = "key-registration";  // 등록 여부
    private final static String KEY_USE_PUSH = "use-push";  // push 사용 여부

    private final static String KEY_USE_CELL_DATA = "use-cell-data";
    private final static String KEY_ALLOW_PERMISSION = "allow-permission";

    /**
     * Push Key 저장
     * @param context   Context
     * @param key   Push Key
     */
    public static void setPushKey(Context context, String key) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_PUSH_KEY, key);
        editor.apply();

        // 등록 여부 초기화
        // setRegistration(context, false);
    }

    /**
     * Push Key 반환
     * @param context   Context
     * @return  Push Key
     */
    public static String getPushKey(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(KEY_PUSH_KEY, "");
    }

    /**
     * Old Push Key 저장
     * @param context   Context
     * @param key   Push Key
     */
    public static void setOldPushKey(Context context, String key) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_OLD_PUSH_KEY, key);
        editor.apply();
    }

    /**
     * Old Push Key 반환
     * @param context   Context
     * @return  Push Key
     */
    public static String getOldPushKey(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(KEY_OLD_PUSH_KEY, "");
    }

    /**
     * User ID 저장
     * @param context   Context
     * @param id    User Id
     */
    public static void setUserId(Context context, String id) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_USER_ID, id);
        editor.apply();
    }

    /**
     * User ID 반환
     * @param context   Context
     * @return  User ID
     */
    public static String getUserId(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(KEY_USER_ID, "");
    }

    /**
     * 등록 여부 저장
     * @param context   Context
     * @param is    등록 여부
     */
    public static void setKeyRegistration(Context context, boolean is) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_REGISTRATION, is);
        editor.apply();
    }

    /**
     * 등록 여부 반환
     * @param context   Context
     * @return  등록 여부
     */
    public static boolean getKeyRegistration(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getBoolean(KEY_REGISTRATION, false);
    }


    /**
     * Push 사용 여부 설정
     * @param is	true: 설정, false: 설정하지 않음
     */
    public static void setUsePush(Context context, boolean is) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_USE_PUSH, is);
        editor.apply();
    }

    /**
     * Push 사용 여부 반환
     * @return	true: 사용, false: 사용하지 않음.
     */
    public static boolean getUsePush(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getBoolean(KEY_USE_PUSH, true);
    }

    /**
     * Cell Data 사용 여부 설정
     * @param context   Context
     * @param is    true: 설정, false: 설정하지 않음
     */
    public static void setUseCellData(Context context, boolean is) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_USE_CELL_DATA, is);
        editor.apply();
    }

    /**
     * Cell Data 사용 여부 반환
     * @param context   Context
     * @return  true: 설정, false: 설정하지 않음
     */
    public static boolean getUseCellData(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getBoolean(KEY_USE_CELL_DATA, false);
    }

    /**
     * 권한 허용 여부 설정
     * @param context   Context
     * @param is    true: 설정, false: 설정하지 않음
     */
    public static void setAllowPermission(Context context, boolean is) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(KEY_ALLOW_PERMISSION, is);
        editor.apply();
    }

    /**
     * 권한 허용 여부 반환
     * @param context   Context
     * @return  true: 설정, false: 설정하지 않음
     */
    public static boolean getAllowPermission(Context context) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getBoolean(KEY_ALLOW_PERMISSION, false);
    }
}
