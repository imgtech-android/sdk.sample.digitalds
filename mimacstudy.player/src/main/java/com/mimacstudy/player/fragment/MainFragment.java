package com.mimacstudy.player.fragment;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Browser;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mimacstudy.player.MainActivity;
import com.mimacstudy.player.R;
import com.mimacstudy.player.listener.IBackPressedListener;

import java.net.URISyntaxException;

public class MainFragment extends Fragment implements IBackPressedListener, View.OnClickListener {

    @SuppressLint("StaticFieldLeak")
    private static MainFragment instance;

    private Button mBtnPlay;
    private Button mBtnDownload;

    private Handler handler = new Handler();    // 토스트 팝업을 위한 핸들러

    /**
     * 생성자
     */
    public MainFragment() {
        super();
    }

    /**
     * MainFragment 반환
     * @return  MainFragment
     */
    public static MainFragment newInstance() {

        if (instance == null) {
            instance = new MainFragment();
        }

        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        initActionBar();

        return initView(inflater);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {

        ((MainActivity)getActivity()).finishApplicationProcess();
    }

    @Override
    public void onClick(View view) {

        if (view == mBtnPlay) {
            testPlayer();
        } else if (view == mBtnDownload) {
            testDownload("guest");
        }
    }

    /**
     * ActionBar 초기화
     */
    @SuppressLint("InflateParams")
    private void initActionBar() {

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_title, null);

        ((TextView) rootView.findViewById(R.id.tv_title)).setText("샘플");

        if (actionBar != null) {
            actionBar.setCustomView(rootView);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private View initView(LayoutInflater inflater) {

        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.fragment_main, null);

        mBtnPlay = (Button) view.findViewById(R.id.btn_play);
        mBtnPlay.setOnClickListener(this);

        mBtnDownload = (Button) view.findViewById(R.id.btn_download);
        mBtnDownload.setOnClickListener(this);

        return view;
    }

    /////
    private void testPlayer() {

        // url scheme string 정의
        String url = "mimacstudy" + "://player?"
                + "siteid=%s"
                + "&userid=%s"
                + "&playid=%s"
                + "&reqversion=%s"
                + "&lmsurl=%s"
                + "&lmsreqtime=%s"
                + "&authurl=%s"
                + "&authreqtime=%s"
                + "&vodurl=%s"
                + "&playtitle=%s"
                + "&durationtime=%s"
                + "&playcurtime=%s"
                + "&studytime=%s"
                + "&progresstime=%s"
                + "&bookmarklist=%s"
                ;

        // url 데이터 설정
        url = String.format(url,
                "mimac-test-site-id",
                "mimac-test-user-id",
                "",
                "100",
                "http://lms-url.co.kr/",
                "30",
                "",
                "",
                "http://idev.imgtech.co.kr/mov/ynstep1_05.mp4",
                "일반 동사 훈련",
                "",
                "",
                "", //data.studyTime,
                "", //data.progressTime,
                "30,75,125"
        );

        // 플레이어 실행
        Intent intent = null;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
            intent.setPackage(getActivity().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {

            // 앱이 설치되어 있지 않으면 Play 스토어 설치 페이지로 전환
            if (intent != null) {
                Uri uri = Uri.parse("market://details?id=kr.imgtech.zoneplayer");
                Intent storeIntent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(storeIntent);
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void testDownload(String userID) {

        // url scheme string 정의
        String url = "mimacstudy" + "://download?"
                + "data=%s"
                ;

        String downloadData =
                "{\"siteid\":\"" + MainActivity.MIMAC_SITE_ID + "\", \"userid\":\"" + userID + "\", \"reqversion\":\"100\","
                + "\"contents\":["
                // 첫번째 다운로드 파일
                + "{\"filename\":\"http://m.imgtech.co.kr/pre/mov/ds/ds_720p.mp4\", \"sitename\":\"대성마이맥\","
                + "\"iscert\":\"0\", \"filetitle\":\"누구나 잘할 수 있는 물리! 안철우 선생님.고화질\", \"totallength\":\"3652\","
                + "\"lectureid\":\"1\",\"lecturename\":\"누구나 잘할 수 있는 물리! 안철우 선생님.고화질\","
                + "\"courseid\":\"A\",\"coursename\":\"안철우 선생님. 물리\","
                + "\"userkey\":\"\", \"etcinfo\":\"\", \"bookmarklist\":\"\"},"
                // 두번째 다운로드 파일
                + "{\"filename\":\"http://m.imgtech.co.kr/pre/mov/ds/ds_360p.mp4\", \"sitename\":\"대성마이맥\","
                + "\"iscert\":\"0\", \"filetitle\":\"누구나 잘할 수 있는 물리! 안철우 선생님.저화질\", \"totallength\":\"3652\","
                + "\"lectureid\":\"2\",\"lecturename\":\"누구나 잘할 수 있는 물리! 안철우 선생님.저화질\","
                + "\"courseid\":\"A\",\"coursename\":\"안철우 선생님. 물리\","
                + "\"userkey\":\"\", \"etcinfo\":\"\", \"bookmarklist\":\"\"}"
                // ...
            +"]}";

        String encodedData = Base64.encodeToString(downloadData.getBytes(), Base64.NO_WRAP);

        url = String.format(url, encodedData);

        // 플레이어(다운로드) 실행
        Intent intent = null;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
            intent.setPackage(getActivity().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {

            // 앱이 설치되어 있지 않으면 Play 스토어 설치 페이지로 전환
            if (intent != null) {
                Uri uri = Uri.parse("market://details?id=kr.imgtech.zoneplayer");
                Intent storeIntent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(storeIntent);
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
