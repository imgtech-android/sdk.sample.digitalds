package com.mimacstudy.player.fragment;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;

import com.mimacstudy.player.MainActivity;
import com.mimacstudy.player.R;
import com.mimacstudy.player.listener.IBackPressedListener;
import com.mimacstudy.player.listener.SingleChoiceDialogListener;
import com.mimacstudy.player.widget.DownloadDialog;
import com.mimacstudy.player.widget.SingleChoiceDialog;

import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.DownloadService3;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.LibUtil;

/**
 * 플레이어 환경설정 Fragment
 * @author kimsanghwan
 * @since 2015. 1. 15.
 */
public class SettingsFragment extends Fragment implements
        BaseInterface
                                                , OnCheckedChangeListener
												, View.OnClickListener
												, SingleChoiceDialogListener
												, IBackPressedListener {

	private Switch mSWWifiOnly;
	private Switch mSWRestoreHardwareSettings;
	private View mViewPlayerMode;
    private TextView mTvPlayerMode;
    private Switch mSWUseExtSd;
    private TextView tvLocation;    // 사용 공간 위치
    private TextView tvUsableSize;	// 사용 가능 공간
    private TextView tvInformation; // 앱 / 버전 정보

	/**
	 * 생성자
	 */
	public SettingsFragment() {
		super();
	}
	
	/**
	 * newInstance
	 * @return	InformationFragment
	 */
	public static SettingsFragment newInstance() {
		
		return new SettingsFragment();
	}

	@SuppressLint({"InflateParams", "SetTextI18n"})
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_settings, null);
		
		mSWWifiOnly = (Switch) view.findViewById(R.id.sw_wifi_only);
		mSWRestoreHardwareSettings = (Switch) view.findViewById(R.id.sw_restore_hardware_settings);
		mViewPlayerMode = view.findViewById(R.id.config_player_mode);
        mTvPlayerMode = (TextView) view.findViewById(R.id.player_mode);
        mSWUseExtSd = (Switch) view.findViewById(R.id.sw_use_extsd);
        tvLocation = (TextView) view.findViewById(R.id.tv_location);
        tvUsableSize = (TextView) view.findViewById(R.id.tv_avail_size);
        tvInformation = (TextView) view.findViewById(R.id.tv_information);

		mViewPlayerMode.setOnClickListener(this);
		
		// Configuration 내용 설정
		mSWWifiOnly.setChecked(!ConfigurationManager.getWiFiOnly(getContext()));
		mSWRestoreHardwareSettings.setChecked(ConfigurationManager.getRestoreHardwareSettings(getContext()));
        if (!ContentFileManager.getInstance(getActivity()).availExtSDCard()) {
            ConfigurationManager.setUseExtSDCard(getContext(), false);
            ContentFileManager.getInstance(getActivity()).setUseExtSDCard(false);
        }
        mSWUseExtSd.setChecked(ConfigurationManager.getUseExtSDCard(getContext()));

		mSWWifiOnly.setOnCheckedChangeListener(this);
		mSWRestoreHardwareSettings.setOnCheckedChangeListener(this);
        mSWUseExtSd.setOnCheckedChangeListener(this);

        mTvPlayerMode.setText(
                getResources().getStringArray(R.array.config_player_mode_option)[ConfigurationManager.getPlayerMode(getContext())]);

        try {
            tvInformation.setText(
                    getResources().getString(R.string.app_name) + " "
                    + (getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        initActionBar();

        setStorageSize();

		return view;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		if (buttonView == mSWWifiOnly) {
			ConfigurationManager.setWiFiOnly(getContext(), !isChecked);
		} else if (buttonView == mSWRestoreHardwareSettings) {
			ConfigurationManager.setRestoreHardwareSettings(getContext(), isChecked);
		} else if (buttonView == mSWUseExtSd) {

            // 다운로드 진행 중 상태 변경 불가 알림
            if (DownloadService3.getActiveDownload() != null) {

                dialogDownloadStatus();

                mSWUseExtSd.setChecked(!isChecked);

                return;
            }

            // 콘텐츠 저장 위치는 실시간 변경 내용을 ContentFileManager 에도 설정 필요
            if (ContentFileManager.getInstance(getActivity()).setUseExtSDCard(isChecked)) {

                ConfigurationManager.setUseExtSDCard(getContext(), isChecked);

                // 콘텐츠 저장 위치 수정 후 재초기화
                ContentFileManager.getInstance(getActivity()).initialize();

                setStorageSize();

            } else {

                ConfigurationManager.setUseExtSDCard(getContext(), false);

                if (getActivity() != null) {
                    LibUtil.toaster(getActivity(), R.string.toast_no_ext_storage);
                }

                mSWUseExtSd.setChecked(false);
            }
        }
	}
	
	@Override
	public void onBackPressed() {

        ((MainActivity)getActivity()).finishApplicationProcess();
	}

	@Override
	public void onClick(View v) {

		if (v == mViewPlayerMode) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                LibUtil.toaster(getActivity(), "통합 플레이어만 사용 가능합니다.");
            } else {
                dialogPlayerMode();
            }

        }
	}

	@Override
	public void onDialog(int state, int selected) {

		switch (state) {
			case SingleChoiceDialog.PLAYER_MODE:

                // 플레이어 모드 설정 저장
				ConfigurationManager.setPlayerMode(getContext(), selected);
                mTvPlayerMode.setText(
                        getResources().getStringArray(R.array.config_player_mode_option)[selected]);

                break;
		}
	}

	/**
	 * ActionBar 초기화
	 */
	@SuppressLint("InflateParams")
	private void initActionBar() {
		
		ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_title, null);

		((TextView) rootView.findViewById(R.id.tv_title)).setText("환경설정");

		if (actionBar != null) {
			actionBar.setCustomView(rootView);
			actionBar.setDisplayShowCustomEnabled(true);
		}
	}

    /**
     * 저장 공간의 전체 및 사용 가능 공간 TextView 설정
     */
    private void setStorageSize() {

        String strCard;
        if (ConfigurationManager.getUseExtSDCard(getContext())) {
            strCard = "[외부저장소]";
        } else {
            strCard = "[내부저장소]";
        }

        if (tvLocation != null) {
            tvLocation.setText(strCard);
        }

        if (tvUsableSize != null) {
            tvUsableSize.setText(String.format("%s GB 사용 가능 / 전체 %s GB", LibUtil.formatFloat(
                            ContentFileManager.getInstance(
                                    getActivity()).getCurrentUsableSpace() / 1024000000f), LibUtil.formatFloat(
                            ContentFileManager.getInstance(
                                    getActivity()).getCurrentTotalSpace() / 1024000000f))
            );
        }
    }

	/**
	 * 플레이어 모드 선택 대화상자
	 */
	private void dialogPlayerMode() {

        SingleChoiceDialog mScDialog = SingleChoiceDialog.getInstance(SingleChoiceDialog.PLAYER_MODE,
                (ConfigurationManager.getPlayerMode(getContext())), this);
		mScDialog.show(getFragmentManager(), SingleChoiceDialog.DIALOG_TAG);
	}

    /**
     * 다운로드 상태로 저장 공간 변경 불가 일림 표출
     */
    private void dialogDownloadStatus() {

        if (getActivity() == null) {
            return;
        }

        String msg = "다운로드 중인 강의가 있으면 저장 공간 변경이 불가능합니다.";

        DownloadDialog mDialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_STATUS, 0, msg, null);
        mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }
}
