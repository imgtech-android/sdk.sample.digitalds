package com.mimacstudy.player.fragment;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimacstudy.player.MainActivity;
import com.mimacstudy.player.R;
import com.mimacstudy.player.itemTouchHelper.ItemTouchHelperAdapter;
import com.mimacstudy.player.itemTouchHelper.OnStartDragListener;
import com.mimacstudy.player.itemTouchHelper.SimpleItemTouchHelperCallback;
import com.mimacstudy.player.listener.CheckBoxChangeListener;
import com.mimacstudy.player.listener.IBackPressedListener;
import com.mimacstudy.player.listener.IModeChangeListener;
import com.mimacstudy.player.widget.ACTION_MODE;
import com.mimacstudy.player.widget.DividerItemDecoration;
import com.mimacstudy.player.widget.DownloadDialog;
import com.mimacstudy.player.widget.ProgressingDialog;

import org.andlib.helpers.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import kr.co.imgtech.lib.zonedrm.CertificateInfo;
import kr.co.imgtech.lib.zonedrm.LibZoneDRM;
import kr.imgtech.lib.zoneplayer.IMGApplication;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.ContentsDatabase2;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.interfaces.CyclicBarrierListener;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadData;
import kr.imgtech.lib.zoneplayer.network.NetworkDefine;
import kr.imgtech.lib.zoneplayer.network.NetworkManager;
import kr.imgtech.lib.zoneplayer.util.BitmapCache;
import kr.imgtech.lib.zoneplayer.util.LibUtil;
import kr.imgtech.lib.zoneplayer.util.StringUtil;
import kr.imgtech.lib.zoneplayer.util.Thumbnailer;
import kr.imgtech.lib.zoneplayer.util.WeakHandler;


/**
 * 콘텐츠 List Fragment
 * @author kimsanghwan
 * @since 2015. 01. 15.
 */
public class ContentsFragment extends Fragment implements BaseInterface
																	, BaseDialogListener
																	, OnClickListener
																	, IntentDataDefine
																	, NetworkDefine
																	, CyclicBarrierListener
																	, OnItemClickListener
																	, IBackPressedListener
                                                                    , OnStartDragListener
																	{

	/*
	 * Handler Message Constants 정의
	 */
    private final static int UPDATE_ITEM = 7;					// 목록 아이템 업데이트

	@SuppressLint("StaticFieldLeak")
	private static ContentsFragment instance;
	private Handler handler;

	private ArrayList<ZoneDownloadData> mListDownload = new ArrayList<>();
	private ContentRecyclerAdapter mAdapter;		// 콘텐츠 리스트 아이템 어댑터(기본)
    private ItemTouchHelper mItemTouchHelper;

	private RecyclerView mContentRecyclerView;					// List

	private View ivSelect;						// 선택 모드 버튼

	private View ivArrowBack;					// 모드 해제 버튼
	private View ivSelectAll;					// 전체 선택 / 선택 해제 버튼
	private View ivDelete;						// 삭제 버튼

	private View mActionModeView;				// ActionModeView
	private Animation leftEnter;				// Animation
	private Animation rightEnter;				// ""
	private IModeChangeListener mModeChangeListener;	// 모드 변경 리스너
	private ACTION_MODE mode = ACTION_MODE.NORMAL;	// Action Mode - 기본 설정(Normal 모드)

	private boolean mViewSelectCheckBox;			// CheckBox 화면 표출 여부
	private boolean mIsAllChecked;					// 전체 선택 및 전체 선택 해제 Flag
	private boolean mNeedAnimation;					// Animation 실행 여부 Flag

	private ContentsDatabase2 mDb;					// 다운로드 아이템 관리 DB 관리자

	private DownloadDialog mDialog;
	private ProgressingDialog mProgressingDialog;

	private Bundle mBundle;
	private Intent mIntent;

	private String mSiteId;
	private String mCourseId;

	/*
	 * Thumbnailer 관련 Member
	 */
    protected final CyclicBarrier mBarrier = new CyclicBarrier(2);
    private Thumbnailer mThumbnailer;
    private ZoneDownloadData mItemToUpdate;		// Thumbnailer 를 통해 전달된 업데이트할 목록 아이템
    private BitmapCache mCache;
    private Point mPoint;						// 썸네일 이미지 사이즈

	/**
	 * 생성자
	 */
	public ContentsFragment() {
		super();
		
		mBundle = null;
	}

	/**
	 * newInstance
	 * @return	ContentFragment
	 */
	public static ContentsFragment newInstance() {
		
		if (instance == null) {
			instance = new ContentsFragment();
		} else {
			instance.mBundle = null;
		}
		
		if (instance.handler == null) {
			instance.handler = new ContentFragmentHandler(instance);
		}
		
		return instance;
	}
	
	/**
	 * newInstance
	 * @param bundle Parameter Bundle
	 * @return	ContentFragment
	 */
	public static ContentsFragment newInstance(Bundle bundle) {
		
		if (instance == null) {
			instance = new ContentsFragment();
		}

        instance.mBundle = bundle;
		
		if (instance.handler == null) {
			instance.handler = new ContentFragmentHandler(instance);
		}
		
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mDb = ContentsDatabase2.getInstance(getActivity());

		mCache = BitmapCache.getInstance(getActivity());
		
		FragmentActivity activity = getActivity();
		mPoint = calcThumbnailSize(Thumbnailer.IMAGE_WIDTH, Thumbnailer.IMAGE_HEIGHT);
		if (activity != null) {
			 mThumbnailer = new Thumbnailer(getActivity(), mPoint.x, mPoint.y);
		}
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (mBundle != null) {
			mIntent = mBundle.getParcelable(MainActivity.KEY_INTENT);
		} else {
			mIntent = null;
		}
		
		initActionBar();
		initAnimation();
		
		return initView(inflater, null);
	}

    @Override
	public void onPause() {
		super.onPause();

		ArrayList<ZoneDownloadData> it = (ArrayList<ZoneDownloadData>) mListDownload.clone();
		new UpdateItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, it);

        // Stop the thumbnailer
        if (mThumbnailer != null) {
            mThumbnailer.stop();
        }
	}

	@Override
    public void onResume() {
        super.onResume();
        
        // 목록 아이템 갱신
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
		}

        updateList();

        // Start the thumbnailer
        if (mThumbnailer != null) {
            mThumbnailer.start(this);
        }

    }

	@Override
	public void onDestroy() {
		super.onDestroy();
		
        if (mThumbnailer != null) {
            mThumbnailer.clearJobs();
        }
        if (mBarrier != null) {
            mBarrier.reset();
        }
        if (mAdapter != null) {
            mAdapter.clear();
        }
        
		releaseDeleteMode();
	}

	@Override
	public void onDialog(int state, int code, int result) {
		
		switch (state) {
		// 삭제 Dialog
		case DIALOG_DOWNLOAD_DELETE_ITEM:
			new DeleteItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			break;

		}
	}

	@Override
	public void onClick(View v) {

		if (v == ivSelect) {
			toggleDeleteMode();
		} else if (v == ivArrowBack) {
			toggleDeleteMode();
		} else if (v == ivDelete) {
			dialogDeleteItem();
		} else if (v == ivSelectAll) {
			mAdapter.setCheckedAll();
		}
	}
	
	@Override
	public void onBackPressed() {

		if (mode == ACTION_MODE.DELETE) {
			
			releaseDeleteMode();
		} else {
			
			Bundle bundle = new Bundle();
			bundle.putParcelable(MainActivity.KEY_INTENT, makeCourseGroupIntent(mSiteId));

			((MainActivity) getActivity()).switchFragment(MainActivity.ID_COURSE_GROUP, bundle);
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (mViewSelectCheckBox) {
            return;
        }

		ZoneDownloadData data = (ZoneDownloadData) parent.getItemAtPosition(position);
		// 저장된 DB (이어보기 / 북마크 등 기타 정보)에서 실행 데이터 획득
		ZoneDownloadData cbData = ContentsDatabase2.getInstance(getActivity()).getFileInfoDetail(data.fileId);
		
		playContent(cbData);
	}
	
    /**
     * Thumbnailer 로부터 업데이트된 ZoneDownloadData 수신
     * Handler 에 UPDATE_ITEM 메시지 전달
     * @param item 업데이트된 ZoneDownloadData
     */
	@Override
    public void setItemToUpdate(ZoneDownloadData item) {
    	
        mItemToUpdate = item;
        handler.sendEmptyMessage(UPDATE_ITEM);
    }
    
    @Override
    public void await() throws InterruptedException, BrokenBarrierException {
        mBarrier.await();
    }

    @Override
    public void resetBarrier() {
        mBarrier.reset();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
	
	/**
	 * View 초기화
	 * @param inflater	LayoutInflater
	 * @param viewGroup	ViewGroup
	 * @return	View
	 */
	private View initView(LayoutInflater inflater, ViewGroup viewGroup) {
		
		View view = inflater.inflate(R.layout.fragment_contents, viewGroup);

        mContentRecyclerView = (RecyclerView) view.findViewById(R.id.rv_content);
        mContentRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        mContentRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mContentRecyclerView.setLayoutManager(mLayoutManager);
        mContentRecyclerView.setItemAnimator(new DefaultItemAnimator());
		
		// Intent 처리
		if (mIntent != null) {
			
			Uri uri = mIntent.getData();
			if (uri != null) {
				
				mSiteId = uri.getQueryParameter(IntentDataDefine.SITE_ID);
				mCourseId = uri.getQueryParameter(IntentDataDefine.COURSE_ID);
			}
		}
		
		initList();
		
		return view;
	}
	
	/**
	 * List 및 Adapter 초기화
	 */
	private void initList() {
		
		if (mContentRecyclerView == null) {
			return;
		}

		if (mAdapter != null) {
			mAdapter.clear();
			mAdapter = null;
		}

        mAdapter = new ContentRecyclerAdapter(this, R.layout.row_contents);
        mModeChangeListener = mAdapter;

        mContentRecyclerView.setAdapter(mAdapter);
        mContentRecyclerView.addOnItemTouchListener(mAdapter);
        if (rightEnter != null) {
            rightEnter.setAnimationListener(mAdapter);
        }

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mContentRecyclerView);
	}
	
	/**
	 * ActionBar 초기화
	 */
	@SuppressLint("InflateParams")
	private void initActionBar() {
		
		ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		
		View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_box, null);
		
		ivSelect = rootView.findViewById(R.id.iv_select);
		ivSelect.setOnClickListener(this);
		ivSelect.setContentDescription("선택모드");

		((TextView) rootView.findViewById(R.id.tv_title)).setText("보관함");

		if (actionBar != null) {
			actionBar.setCustomView(rootView);
			actionBar.setDisplayShowCustomEnabled(true);

			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
		}
	}
	
	/**
	 * Transition Animation 초기화
	 */
	private void initAnimation() {
		
		leftEnter = AnimationUtils.loadAnimation(getActivity(), R.anim.left_enter);
		rightEnter = AnimationUtils.loadAnimation(getActivity(), R.anim.right_enter);
	}
	
	/**
	 * 삭제 ActionMode 로 전환
	 */
	@SuppressLint("InflateParams")
	private void setDeleteMode() {
		
		mode = ACTION_MODE.DELETE;

		mActionModeView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_box_actionmode, null);

		ivArrowBack = mActionModeView.findViewById(R.id.iv_arrow_back);
		ivArrowBack.setOnClickListener(this);
		ivSelectAll = mActionModeView.findViewById(R.id.iv_select_all);
		ivSelectAll.setOnClickListener(this);
		ivDelete = mActionModeView.findViewById(R.id.iv_delete);
		ivDelete.setOnClickListener(this);
		
		((MainActivity) getActivity()).setActionModeView(mActionModeView);

		ivArrowBack.startAnimation(leftEnter);
		ivSelectAll.startAnimation(rightEnter);
		ivDelete.startAnimation(rightEnter);
		
		mNeedAnimation = true;
		mModeChangeListener.changeMode(mode);
	}
	
	/**
	 * 삭제 ActionMode 해제
	 */
	private void releaseDeleteMode() {
		
		mode = ACTION_MODE.NORMAL;
		
        ((MainActivity) getActivity()).releaseActionModeView(mActionModeView);

        ivArrowBack = null;
        ivDelete = null;
        ivSelectAll = null;
		mActionModeView = null;

		if (mModeChangeListener != null) {
			mModeChangeListener.changeMode(mode);
		}
	}
	
	/**
	 * ActionBar 를 Normal - Delete 모드로 전환
	 */
	private void toggleDeleteMode() {
		
		if (mode == ACTION_MODE.NORMAL) {

			setDeleteMode();
		} else {

			releaseDeleteMode();
		}
	}
	
	/**
	 * Today 0시 0분 0초 시간(TimeInMillis) 반환
	 * @return	Today 0시 0분 0초 시간(TimeInMillis)
	 */
	private long getTodayZeroHour() {
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		
		int year = cal.get(Calendar.YEAR);
		int mon = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		
		cal.set(year, mon, day, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTimeInMillis();
	}
	
	/**
	 * Time 을 날짜-시간 형식 문자로 변환
	 * @param date	Time (초 단위)
	 * @return	날짜-시간 형식 문자열
	 */
	private String getFormatDateTime(long date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date * 1000);
		
		int year = cal.get(Calendar.YEAR);
		int mon = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);

		return String.format("%04d.%02d.%02d %02d:%02d:%02d", year, mon + 1, day, hour, min, sec);
	}
	
	/**
	 * 특정 원하는 날짜의 Date 반환
	 * example) fakeSetDate(2014, 9, 24, 10, 30, 25) -> 2014년 9월 24일 10시 30분 25초
	 * @param year		설정을 원하는 연도
	 * @param month		설정을 원하는 월
	 * @param date		설정을 원하는 일
	 * @param hour		설정을 원하는 시
	 * @param minute	설정을 원하는 분
	 * @param second	설정을 원하는 초
	 * @return	Date
	 */
	private Date fakeSetDate(int year, int month, int date, int hour, int minute, int second) {
		
		Calendar cal = Calendar.getInstance();
		
		cal.set(year, (month - 1), date, hour, minute, second);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	private Date getDateAddMonth(int addMonth) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, addMonth);
		
		return cal.getTime();
	}
	
	private Date getDateAddDay(int addDay) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, addDay);
		
		return cal.getTime();
	}
	
	/**
	 * Time 을 날짜 형식 문자로 반환
	 * @param date	Time (초 단위)
	 * @return 날짜 형식 문자열
	 */
	private String getFormatDate(long date) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date * 1000);
		
		int year = cal.get(Calendar.YEAR);
		int mon = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);

		return String.format("%04d.%02d.%02d", year, mon + 1, day);
	}
	
	/**
	 * double 데이터를 소수점 x자리 문자열로 변환
	 * @param size	소수점 자리 수
	 * @return	변환된 문자열
	 */
    private String formatRateDouble(double size) {
        return String.format(java.util.Locale.US, "%.1f", size);
    }
	
	/**
	 * ArrayList 타입의 북마크 데이터를 ","를 구분자로 갖는 String 으로 반환
	 * @param arrayList	북마크 ArrayList
	 * @return	북마크 String
	 */
	private String getBookmarkString(ArrayList<String> arrayList) {
		
		StringBuilder builder = new StringBuilder();
		
		Iterator<String> it = arrayList.iterator();
		while (it.hasNext()) {
			builder.append(it.next());
			if (it.hasNext()) {
				builder.append(",");
			}
		}
		
		return builder.toString();
	}

    /**
     * 다운로드 데이터를 재생 데이터 형식으로 변환하여 반환
     * @param data	ZoneDownloadData
     * @param playUrl 미디어 재생 URL
     * @return	ZonePlayerData String
     */
    private String convertZonePlayerData(ZoneDownloadData data, String playUrl) {

 		/*
		 * Activity 에 Attach 되어 있지않으면
		 * getResources()를 실행할 수 없으며, 플레이어 재생이 불가능하므로
		 * 실행하지 않고 null 반환
		 */
		if (! isAdded()) {
			return null;
		}

		String ret = "mimacstudy" + "://player?"
				+ "siteid=%s"
				+ "&userid=%s"
				+ "&playid=%s"
				+ "&reqversion="
				+ "&lmsurl=%s"
				+ "&lmsreqtime=%s"
				+ "&authurl=%s"
				+ "&authreqtime=%s"
				+ "&mrl=%s"
				+ "&playtitle=%s"
				+ "&durationtime=%s"
				+ "&playcurtime=%s"
				+ "&studytime=%s"
				+ "&progresstime=%s"
				+ "&bookmarklist=%s"
				+ "&extinfo=%s"
				+ "&islocalplay=1"
				+ "&fileid=%s"
				+ "&lmsid=%s"
				+ "&drmauthurl=%s"
				+ "&drmauthtime=%s"
				+ "&drmauthid=%s"
				+ "&deviceinfo=%s"
				+ "&subtitlespath=%s"
				+ "&subtitlescharset=%s"
				+ "&courseid=%s"
				+ "&coursename=%s"
				+ "&courseseq=%s"
				+ "&teachername=%s"
				+ "&lectureid=%s"
				+ "&lecturename=%s"
				+ "&lectureseq=%s"
				+ "&etcinfo=%s"
				;

		// extinfo 는 URL Encoding(UTF-8)
		String encodedExtInfo = "";
		if (data.extInfo != null) {
			try {
				encodedExtInfo = URLEncoder.encode(data.extInfo, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		// 이어보기 -5초 처리(이전 재생 시점 인지 처리)
		int intPlayCurTime;
		try {
			intPlayCurTime = Integer.parseInt(data.playCurTime);
		} catch (NumberFormatException e) {
			intPlayCurTime = 0;
		}

		if (intPlayCurTime > 5) {
			intPlayCurTime -= 5;
		}

		return String.format(ret,
				data.siteId,
				data.userId,
				data.playId,
				data.lmsUrl,
				data.lmsReqTime,
				data.authUrl,
				data.authReqTime,
				playUrl,
				data.fileTitle,
				data.totalLength,
				Integer.toString(intPlayCurTime),
				"", //data.studyTime,
				"", //data.progressTime,
				getBookmarkString(data.arrayListBookmark),
				encodedExtInfo,
				data.fileId,
				data.lmsId,
				data.drmAuthUrl,
				data.drmAuthTime,
				data.drmAuthId,
				LibUtil.getAndroidId(getActivity().getApplicationContext()),
				data.subtitlesPath,
				data.subtitlesCharset,
				data.courseId,
				data.courseName,
				data.courseIndex,
				data.teacherName,
				data.lectureId,
				data.lectureName,
				data.lectureIndex,
				encodedExtInfo
		);
    }
	
	/**
	 * 다운로드 완료된 파일 재생
	 * @param data	ZoneDownloadData
	 */
	private void playContent(ZoneDownloadData data) {

        // 현재 콘텐츠 파일 path 가 내장 또는 외장인지 확인
        String path =
                (StringUtil.contains(data.filePath,
                        ContentFileManager.getInstance(getContext()).mExtSDCardPath)
                        ? ContentFileManager.getInstance(getContext()).mExtSDCardPath
                        : ContentFileManager.getInstance(getContext()).mIntSDCardPath);

        // 외장 유효 여부 확인 후 유효하지 않으면 오류 Alert 표출
        if (! ContentFileManager.getInstance(getContext()).isAvailSpace(path)) {

            dialogNoExtSDCard();
            return;
        }

		// 인증서 유효 기간 비교. 인증서 유효 기간에 포함되는지 확인
		// 인증서 필요 여부 비교. 인증서가 필요없는 파일인지 확인
		if (! isValidCert(data.filePath)
			&& (data.isCert > 0)) {
			dialogInvalidCertTerm();
			return;
		}

		// 인증 파일이면 ZoneHttpd 를 설정하고 해당 URL 획득
		if (data.isCert > 0 ) {

			// 기기인증 후 재생
			new DRMAuthDeviceAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);
		
		// 미인증 파일이면 기본 URL 앞에 file scheme 을  추가
		} else {
			
			// 다운로드 데이터의 재생 경로 변환. ZoneHttpd 설정 포함
			String filepath;
			filepath = "file://" + data.filePath;
			
			// 다운로드 데이터를 재생 데이터 형식으로 변환
			String url = convertZonePlayerData(data, filepath);
			
			// ZonePlayerData 가 없으면 실행하지 않고 종료
			if (! StringUtil.isNotBlank(url)) {
				return;
			}
			
			Intent intent = null;
			try {
				// Intent Scheme 실행
				intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

				intent.addCategory(Intent.CATEGORY_BROWSABLE);
	        	intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
				intent.setPackage(getActivity().getPackageName());
				
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				
				// 앱이 설치되어 있지 않으면 Play 스토어 설치 페이지로 전환
				if (intent != null) {
					Uri uri = Uri.parse(getResources().getString(R.string.url_playstore) + intent.getPackage());
					Intent storeIntent = new Intent(Intent.ACTION_VIEW, uri);
					startActivity(storeIntent);
				}

			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 인증서 유효시간 및 현재시간 비교
	 * @param filePath	인증서를 확인할 콘텐츠 파일의 저장 경로
	 * @return	true: 유효, false: 유효하지 않음
	 */
	private boolean isValidCert(String filePath) {

		LibZoneDRM libZoneDRM = new LibZoneDRM(getContext());
		CertificateInfo ci = libZoneDRM.getLocalCertificate(filePath);

		/*LibCertificate certificate = new LibCertificate(IMGApplication.getAppContext());
		CertificateInfo ci = certificate.getLocalCertificate(filePath);*/
		
		// 인증서가 없으면 재생 취소
		if (ci == null) {
			return false;
		}
		
		long current = getTodayZeroHour() / 1000;			// Today 0시 0분 0초 초단위 시간
//		long current = System.currentTimeMillis() / 1000;	// 현재 초단위 시간
//		long current = getDateAddDay(2).getTime() / 1000;	// 유효기간 Fake 테스트
		
		// 현재 시간이 유효 시작 시간보다 크고, 유효 종료 시간보다 작으면
		// true 반환. 아니면 false 반환
		/*(current > ci.startDate) && */
		return (current < ci.endDate);
	}
	
    /**
     * Thumbnailer 로부터 전달된 업데이트된 목록 아이템을 ArrayAdapter 에 갱신 요청
     */
    private void updateItem() {
    	
    	mAdapter.update(mItemToUpdate);
    	
        try {
            mBarrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
			e.printStackTrace();
        }
    }
	
	/**
	 * 목록 표출/업데이트
	 */
	private void updateList() {
	
	    if (mThumbnailer != null) {
	        mThumbnailer.clearJobs();
	    }

        ArrayList<ZoneDownloadData> listDownload;
		if (StringUtil.isNotBlank(mCourseId)) {
            listDownload = mDb.getAllFileInfoDetail(mSiteId, mCourseId);
		} else {
            listDownload = mDb.getAllFileInfoDetail(mSiteId);
		}
	    
	    mListDownload.clear();

        // 목록이 없으면 편집 선택 버튼 비활성화
        if ((listDownload != null) && (listDownload.size() == 0)) {
            ivSelect.setVisibility(View.GONE);
        }
	    
		if (listDownload.size() > 0) {
			
			for (ZoneDownloadData item : listDownload) {
				
				// 캐쉬에 썸네일이 있으면
				Bitmap thumbnail = mCache.getBitmapFromMemCache(item.filePath);
				if (thumbnail != null) {
					
					item.pictureParsed = true;
					item.picture = thumbnail;
					mAdapter.addItem(item);
				
				// 캐쉬에 썸네일이 없으면
				} else {
					
					// DRM 콘텐츠이면
					if (item.isCert > 0) {
						item.pictureParsed = true;
						
						if (StringUtil.isNotBlank(item.thumbnailPath)) {
							item.picture = getThumbnailFromContextDir(item.thumbnailPath);
						}

						mAdapter.addItem(item);
						
					// 일반 콘텐츠이면
					} else {
						
						// item.playUrl = "file://" + item.filePath;
						// item.playUrl = getRealPathFromURI(Uri.parse(item.playUrl));
						item.playUrl = item.filePath;

						mAdapter.addItem(item);

						if (mThumbnailer != null) {
							mThumbnailer.addJob(item);
						}

					}
				}
			}
		} else {
			
			dialogNoContents();
		}
	
		mModeChangeListener = mAdapter;
	}

    /**
     * 삭제 Alert 표출
     */
    private void dialogDeleteItem() {

        if (getActivity() == null) {
            return;
        }

        if (mAdapter.getSelectList().contains(true)) {

            mDialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_DELETE_ITEM, 0, "콘텐츠", this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        } else {

            dialogNoSelect();
        }
    }

    /**
     * 선택 강좌 없음 알림 표출
     */
    private void dialogNoSelect() {

        if (getActivity() == null) {
            return;
        }

        String msg = "선택한 콘텐츠가 없습니다.";

        mDialog = DownloadDialog.getInstance(DIALOG_NO_SELECT, 0, msg, this);
        mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

    /**
     * 다운로드된 콘텐츠 없음 Alert 표출
     */
    private void dialogNoContents() {

        if (getActivity() == null) {
            return;
        }

    	mDialog = DownloadDialog.getInstance(DIALOG_NO_DOWNLOAD_CONTENTS, this);
    	mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }
    
    /**
     * 인증서 기간이 유효하지 않음 Alert 표출
     */
    private void dialogInvalidCertTerm() {

        if (getActivity() == null) {
            return;
        }

    	mDialog = DownloadDialog.getInstance(DIALOG_INVALID_CERT_TERM, this);
    	mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }
    
    /**
     * 미인증 단말기 Alert 표출
     * @param message Alert 에 표출할 메시지
     */
    private void dialogInvalidDevice(String message) {

        if (getActivity() == null) {
            return;
        }

    	if (StringUtil.isNotBlank(message)) {
        	mDialog = DownloadDialog.getInstance(DIALOG_DRM_ERROR, 0, message, this);
		} else {
			mDialog = DownloadDialog.getInstance(DIALOG_DRM_ERROR, 0,
					getActivity().getResources().getString(R.string.dialog_message_invalid_device), this);
		}

    	mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }

	/**
	 * 외장 SD 카드 없음 Alert 표출
	 */
	private void dialogNoExtSDCard() {

        if (getActivity() == null) {
            return;
        }

		mDialog = DownloadDialog.getInstance(DIALOG_NO_EXT_SDCARD, this);
		mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
	}

    /**
     * ProgressDialog 표출
     */
    private void dialogWaiting() {

        if (getActivity() == null) {
            return;
        }

    	mProgressingDialog = ProgressingDialog.getInstance(0);
    	mProgressingDialog.show(getActivity().getSupportFragmentManager(), ProgressingDialog.DIALOG_TAG);
    }
    
    /**
     * ProgressDialog 해제
     */
    private void dismissWaiting() {

        if (getActivity() == null) {
            return;
        }

    	if (mProgressingDialog != null) {
    		mProgressingDialog.dismiss();
    		mProgressingDialog = null;
		}
    }
    
	/**
	 * 각 단말기기에 맞는 썸네일 크기 계산 반환
	 * @param rw	설정할 이미지 폭
	 * @param rh	설정할 이미지 높이
	 * @return	계산된 폭/높이가 설정된 Point
	 */
	private Point calcThumbnailSize(int rw, int rh) {
		
		FragmentActivity activity = getActivity();
		Display display = activity.getWindowManager().getDefaultDisplay();
		
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        
        int width = rw, height = rh;
        
        // 썸네일 크기 조정
        if (LibUtil.isTablet(activity)) {
        	width *= 1.5f;
        	height *= 1.5f;
		} else {
			width *= (metrics.density * 0.5f);
			height *= (metrics.density * 0.5f);
		}
        
        return new Point(width, height);
	}
	
	/**
	 * Course Group Fragment Intent 생성
	 * @param siteId	Site ID
	 * @return	Course Group Fragment Intent
	 */
	private Intent makeCourseGroupIntent(String siteId) {
		
		Intent intent = null;
		
		String scheme = "zoneplayer";

		String uri = String.format((scheme + "://courselist?"
                        + "siteid=%s"
                        + "&courseid=%s"),
                siteId,
                "test")
				;
		
		// Intent 설정
		try {
			intent = Intent.parseUri(uri, Intent.URI_INTENT_SCHEME);
			intent.addCategory(Intent.CATEGORY_BROWSABLE);
        	intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
			intent.setPackage(getActivity().getPackageName());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		return intent;
	}
	
	/**
	 * Context Directory 내 썸네일 디렉터리에 저장된 Bitmap 획득
	 * @param path	이미지 파일 경로
	 * @return	썸네일 Bitmap
	 */
	private Bitmap getThumbnailFromContextDir(String path) {
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		Bitmap originBitmap = BitmapFactory.decodeFile(path, options);
		
		/*Logger.d("origin width: " + options.outWidth);
		Logger.d("origin height: " + options.outHeight);
		
		Logger.d("mPoint.w: " + mPoint.x);
		Logger.d("mPoint.h: " + mPoint.y);*/
		
		if (originBitmap != null) {
			return Bitmap.createScaledBitmap(originBitmap, mPoint.x, mPoint.y, false);
		} else {
			return null;
		}
	}

    public String getRealPathFromURI(Uri contentUri) {

        Cursor cursor = getActivity().managedQuery(
                contentUri, new String[] {MediaStore.Images.Media.DATA, }, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private class ContentRecyclerAdapter extends RecyclerView.Adapter<ViewHolder>
                                        implements IModeChangeListener
                                        , RecyclerView.OnItemTouchListener
                                        , ItemTouchHelperAdapter
                                        , AnimationListener {

        private final OnStartDragListener onStartDragListener;
        private int itemLayout;
		
    	// CheckBox 체크 여부 저장 ArrayList
    	private ArrayList<Boolean> mListContentSelect = new ArrayList<>();
		

		ContentRecyclerAdapter(OnStartDragListener dragListener, int layout) {
			
			super();

            onStartDragListener = dragListener;
            itemLayout = layout;
			
			initCheckedList(false);
		}

        @Override
        public int getItemCount() {

            if (mListDownload != null) {
                return mListDownload.size();
            } else {
                return 0;
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout, viewGroup, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {

            ZoneDownloadData data = mListDownload.get(position);

            data.lectureIndex = position;

            // 파일 타이틀 설정
            holder.tvTitle.setText(data.fileTitle);

            // 파일 생성날짜 / 크기 설정
            double size;
            try {
                size = Double.parseDouble(data.fileSize);
            } catch (NumberFormatException e) {
                size = 0.0f;
            }
            holder.tvTimeSize.setText(
                    String.format("%s / %s MB", getFormatDate(data.createTime), formatRateDouble(size / (double) 1024000)));

            // 유효기간 / 재생 길이 설정
            long length = 0L;
            if (StringUtil.isNotBlank(data.totalLength)) {
                try {
                    length = Long.parseLong(data.totalLength);
                } catch (NumberFormatException e) {
                    length = 0L;
                }
            }

            if (data.isCert > 0) {

                if (data.picture != null) {
                    holder.ivIcon.setImageBitmap(data.picture);
                } else {
                    holder.ivIcon.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_slideshow_g));
                }

                holder.tvTermLength.setText(
                        String.format("%s - %s / %s",
                                getFormatDate(data.startTime), getFormatDate(data.endTime), LibUtil.millisToString(length * 1000)));

            } else {

                if (data.picture != null) {
                    holder.ivIcon.setImageBitmap(data.picture);
                } else {
                    holder.ivIcon.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_slideshow_g));
                }

                holder.tvTermLength.setText(
                        String.format("기간 제한없음 / %s", LibUtil.millisToString(length * 1000)));
            }

            // 기타 정보 설정
            if (StringUtil.isNotBlank(data.fileInfo)) {
                holder.tvInfo.setText(data.fileInfo);
            }

            // 저장 위치 설정
            if (StringUtil.contains(data.filePath,
                    ContentFileManager.getInstance(getContext()).mExtSDCardPath)) {
                holder.tvLocation.setText("외부");
            } else {
                holder.tvLocation.setText("내부");
            }

            // 삭제 ActionMode 여부에 따라 CheckBox 표출 여부 설정
            if (mViewSelectCheckBox) {

                if (mNeedAnimation) {
                    holder.cbSelect.startAnimation(rightEnter);
                }
                holder.cbSelect.setVisibility(View.VISIBLE);
				holder.handleView.setVisibility(View.GONE);
            } else {

                holder.cbSelect.setVisibility(View.GONE);
				holder.handleView.setVisibility(View.VISIBLE);
            }

            holder.cbSelect.setOnCheckedChangeListener(
                    new CheckBoxChangeListener(mListContentSelect, position));
            holder.cbSelect.setChecked(mListContentSelect.get(position));

            holder.handleView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        onStartDragListener.onStartDrag(holder);
                    }
                    return false;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView arg0, MotionEvent arg1) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView arg0, MotionEvent arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

        @Override
        public void changeMode(ACTION_MODE mode) {

            if (mode == ACTION_MODE.NORMAL) {
                changeCheckBox(false);
            } else if (mode == ACTION_MODE.DELETE) {
                changeCheckBox(true);
            }
        }

        @Override
        public boolean onItemMove(int fromPosition, int toPosition) {

            // 다운로드 요청 리스트 swap
            Collections.swap(mListDownload, fromPosition, toPosition);

            notifyItemMoved(fromPosition, toPosition);

            return true;
        }

        @Override
        public void onItemDismiss(int position) {

        }

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mNeedAnimation = false;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        /**
         * ZoneDownloadData Item 반환
         * @param position	Item Position
         * @return	ZoneDownloadData
         */
        ZoneDownloadData getItem(int position) {

            if (mListDownload != null) {
                return mListDownload.get(position);
            } else {
                return null;
            }
        }

		@Override
		public long getItemId(final int position) {
			return position;
		}

		void addItem(ZoneDownloadData item) {
            mListDownload.add(item);
            notifyItemInserted(mListDownload.size() - 1);

			initCheckedList(false);
		}

		void addItem(ZoneDownloadData item, int index) {

            if (index > mListDownload.size()) {
                index = mListDownload.size();
            }
            mListDownload.add(index, item);
            notifyItemInserted(index);

            initCheckedList(false);
        }

		void removeItem(ZoneDownloadData item) {

            int position = mListDownload.indexOf(item);
            mListDownload.remove(position);
            notifyItemRemoved(position);

			initCheckedList(false);
		}
		
		public synchronized void update(ZoneDownloadData item) {

            int index = item.lectureIndex;

			if (index > -1) {
                removeItem(item);
				addItem(item, index);
			}
		}

        public synchronized void update(ZoneDownloadData item, int index) {

            if (index > -1) {
                removeItem(item);
                addItem(item, index);
            }
        }

        /**
         * 전체 Adapter List 초기화
         */
		void clear() {

            if (mListDownload != null) {
                mListDownload.clear();
            }
        }
		
		/**
		 * Delete CheckBox 화면 표출 여부 설정
		 * @param isView	true: 표출, false: otherwise
		 */
		private void changeCheckBox(boolean isView) {

			mViewSelectCheckBox = isView;
			
			notifyDataSetChanged();
		}
		
		/**
		 * CheckBox List 초기화
		 * @param value	초기화할 기본
		 */
		private void initCheckedList(boolean value) {
			
			int count = getItemCount();
			mListContentSelect.clear();
			
			for (int i = 0; i < count; i++) {
				
				mListContentSelect.add(value);
			}
		}
		
		/**
		 * CheckBox 전체 선택 / 해제
		 */
		private void setCheckedAll() {
			
			mIsAllChecked = (! mIsAllChecked);
			
			int count = getItemCount();

            for (int i = 0; i < count; i++) {
				
				mListContentSelect.set(i, mIsAllChecked);
			}
			
			notifyDataSetChanged();
		}
		
		/**
		 * Select List 반환
		 * @return	선택 List
		 */
		private ArrayList<Boolean> getSelectList() {
			
			return mListContentSelect;
		}
    }
	
    /**
	 * ViewHolder
	 * @author kimsanghwan
	 * @since  2015. 1. 22.
	 *
	 */
	class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

		private ImageView ivIcon;
		private TextView tvTitle;
		private TextView tvTimeSize;
		private TextView tvTermLength;
		private TextView tvInfo;
        private View handleView;
		private CheckBox cbSelect;
        private TextView tvLocation;

        /**
         * 생성자
         * @param itemView	ItemView
         */
		ViewHolder(View itemView) {

            super(itemView);

            ivIcon = (ImageView) itemView.findViewById(R.id.iv_icon);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvTimeSize = (TextView) itemView.findViewById(R.id.tv_time_size);
            tvTermLength = (TextView) itemView.findViewById(R.id.tv_term_length);
            tvInfo = (TextView) itemView.findViewById(R.id.tv_info);
            handleView = itemView.findViewById(R.id.iv_handle);
            cbSelect = (CheckBox) itemView.findViewById(R.id.cb_select);
            tvLocation = (TextView) itemView.findViewById(R.id.tv_location);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (mViewSelectCheckBox) {
                return;
            }

            ZoneDownloadData data = mListDownload.get(getLayoutPosition());
            // 저장된 DB (이어보기 / 북마크 등 기타 정보)에서 실행 데이터 획득
            ZoneDownloadData cbData = ContentsDatabase2.getInstance(getActivity()).getFileInfoDetail(data.fileId);

            playContent(cbData);
        }
    }
	
	private static class ContentFragmentHandler extends WeakHandler<ContentsFragment> {

		ContentFragmentHandler(ContentsFragment owner) {
			super(owner);
		}

		@Override
		public void handleMessage(Message msg) {

            ContentsFragment owner = getOwner();
			
			switch (msg.what) {
			
			// 목록 아이템 업데이트
			case UPDATE_ITEM:
				
				owner.updateItem();
				break;
			}
		}
	}

    /**
     * 정렬 순서 업데이트 AsyncTask
     */
    private class UpdateItemAsyncTask extends AsyncTask<ArrayList<ZoneDownloadData>, Void, Void> {

        @SafeVarargs
		@Override
        protected final Void doInBackground(ArrayList<ZoneDownloadData>... params) {

            int index = 0;

            // 강좌 그룹
            if (StringUtil.isNotBlank(mCourseId)) {

                for (ZoneDownloadData data : params[0]) {

                    data.lectureIndex = index++;
                    mDb.updateFileInfoIndex(data, false);
                }
            // All
            } else {

                for (ZoneDownloadData data : params[0]) {

                    data.allIndex = index++;
                    mDb.updateFileInfoIndex(data, true);
                }
            }

            return null;
        }
    }
	
	/**
	 * 선택 콘텐츠 삭제 AsyncTask
	 * @author kimsanghwan
	 * @since 2014. 10. 23.
	 */
	private class DeleteItemAsyncTask extends AsyncTask<Void, Void, ArrayList<ZoneDownloadData>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dialogWaiting();
			toggleDeleteMode();	// 삭제 처리가 시작되면 Action Mode 원복
		}

		@Override
		protected ArrayList<ZoneDownloadData> doInBackground(Void... params) {

			ArrayList<ZoneDownloadData> listDelete = new ArrayList<>();

			int size = mAdapter.getItemCount();
			ArrayList<Boolean> listSelect = mAdapter.getSelectList();

            // 선택된 Item 이 있으면 실행
            if (listSelect.contains(true)) {

                // 삭제 일괄 처리
                for (int i = 0; i < size; i++) {

                    // 선택된 Item 이면 처리 실행
                    if (listSelect.get(i)) {

                        // 삭제할 아이템 획득
                        ZoneDownloadData data = mAdapter.getItem(i);

                        if ((!ContentFileManager.getInstance(getActivity()).availExtSDCard())
                                && (StringUtil.contains(data.filePath, ContentFileManager.getInstance(getContext()).mExtSDCardPath))) {

                            dialogNoExtSDCard();
                            return null;
                        }

						/*
						 * 삭제 리스트에 추가
						 * AsyncTask 에서는 View 에 변경 이벤트를 전달할 수 없으므로
						 * 별도 삭제 리스트에 추가 후 onPostExecute 에서 처리
						 */
                        listDelete.add(data);

                        // Item 별 처리 delay
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            // 삭제 진행
            for (ZoneDownloadData data : listDelete) {

                // 파일 삭제
                ContentFileManager.getInstance(getActivity()).deleteFile(data.filePath);
                // DB 삭제 처리
                ContentsDatabase2.getInstance(getActivity()).deleteFileInfoDetail(data);

                // Item 별 처리 delay
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

			return listDelete;
		}

		@Override
		protected void onPostExecute(ArrayList<ZoneDownloadData> result) {
			super.onPostExecute(result);

            if (result != null) {

                if (result.size() == mAdapter.getItemCount()) {

                    // DB 에서 강좌 그룹 삭제 처리
                    ContentsDatabase2.getInstance(getActivity()).deleteCourseInfo(mSiteId, mCourseId);

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(MainActivity.KEY_INTENT, makeCourseGroupIntent(mSiteId));

                    ((MainActivity) getActivity()).switchFragment(MainActivity.ID_COURSE_GROUP, bundle);

                    dismissWaiting();
                    return;
                }

                // Adapter 삭제
                for (ZoneDownloadData data : result) {
                    mAdapter.removeItem(data);
                }

                // Select ArrayList 초기화
                mAdapter.initCheckedList(false);
            }

            dismissWaiting();
		}
	}

	/**
	 * DRM 기기인증 AsyncTask
	 * @author kimsanghwan
	 * @since 2014. 11. 14.
	 */
	private class DRMAuthDeviceAsyncTask extends AsyncTask<ZoneDownloadData, Void, JSONObject> {
		
		private ZoneDownloadData data;

		@Override
		protected JSONObject doInBackground(ZoneDownloadData... params) {

			if ((params != null) && (params[0] != null)) {
				
				data = params[0];
				
				NetworkManager netMgr = new NetworkManager(getContext());
				
				String url = makeDRMAuthUrl(params[0]);
				
				if (url != null) {
					
					try {
						String res = netMgr.httpGet(url);
						
						JSONObject retJson;
						try {
							retJson = new JSONObject(res);
						} catch (JSONException e) {
							//Logger.d("drm auth response: " + res);
							return null;
						}

						return retJson;
					} catch (Exception e) {
						e.printStackTrace();
						return null;
					}
				}

			}

			return null;
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			super.onPostExecute(result);
			
			parseDRMAuthResponse(result);
		}
		
		/**
		 * DRM 기기인증을 위한 전체 URL 생성 반환
		 * @param data	ZoneDownloadData
		 * @return	DRM 기기인증을 위한 전체 URL
		 */
		private String makeDRMAuthUrl(ZoneDownloadData data) {
			
			String ret = null;
			String token = "?";
			
			if ((data != null)
					&& (StringUtil.isNotBlank(data.userId))
					&& (StringUtil.isNotBlank(data.drmAuthUrl))
					&& (StringUtil.isNotBlank(data.drmAuthId))) {

				if (data.drmAuthUrl.contains(token)) {
					token = "&";	// 기존 URL 에 연결된 데이터가 있으면 연결자 변경
				}

                // drmAuthId 에 한글이 있는 경우에 대한 처리.
                String encodedDrmAuthId;
                try {
                    encodedDrmAuthId = URLEncoder.encode(data.drmAuthId, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    encodedDrmAuthId = data.drmAuthId;
                }
				
				// 서버에 재생 정보 요청 URL 생성
				ret = data.drmAuthUrl 
						+ token + USER_ID + "=" + data.userId
						+ "&" + DRM_AUTH_ID + "=" + encodedDrmAuthId
						+ "&" + DEVICE_INFO + "=" + LibUtil.getAndroidId(getActivity())
						+ "&" + STATUS + "=0";	// 재생전(0)
			}

			return ret;
		}
		
		/**
		 * DRM 기기인증 응답 처리
		 * @param json	인증 응답 JSONObject
		 */
		private void parseDRMAuthResponse(JSONObject json) {
			
			if (json != null) {
				
				String code = null, message = null;
				
				try {
					if (json.has(RESPONSE_CODE)) {
						code = json.getString(RESPONSE_CODE);
					}
					if (json.has(RESPONSE_MESSAGE)) {
						message = json.getString(RESPONSE_MESSAGE);
					}
					
					if (StringUtil.equals(code, "0")) {
						dialogInvalidDevice(message);
						return;
					}
					
				} catch (JSONException e) {
					Logger.d("drm auth response json fail: " + json.toString());
				}
			}

			//String filepath = setZoneHttpDContentUrl(data.filePath);
			String filepath =
					"http://localhost:" + IMGApplication.mZoneHttpDPort + data.filePath;
			
			// 다운로드 데이터를 재생 데이터 형식으로 변환
			String url = convertZonePlayerData(data, filepath);
			
			// ZonePlayerData 가 없으면 실행하지 않고 종료
			if (! StringUtil.isNotBlank(url)) {
				return;
			}

			Intent intent = null;
			// Intent Scheme 실행
			try {
				intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

			if (intent != null) {
				try {

					intent.addCategory(Intent.CATEGORY_BROWSABLE);
					intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
					intent.setPackage(getActivity().getPackageName());

					startActivity(intent);
				} catch (ActivityNotFoundException e) {

					// 앱이 설치되어 있지 않으면 Play 스토어 설치 페이지로 전환
					Uri uri = Uri.parse(getResources().getString(R.string.url_playstore) + intent.getPackage());
					Intent storeIntent = new Intent(Intent.ACTION_VIEW, uri);
					startActivity(storeIntent);
				}
			}

		}
	}

}
