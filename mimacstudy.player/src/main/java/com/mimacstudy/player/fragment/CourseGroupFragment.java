package com.mimacstudy.player.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Browser;
import android.support.v4.app.Fragment;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimacstudy.player.MainActivity;
import com.mimacstudy.player.R;
import com.mimacstudy.player.itemTouchHelper.ItemTouchHelperAdapter;
import com.mimacstudy.player.itemTouchHelper.OnStartDragListener;
import com.mimacstudy.player.itemTouchHelper.SimpleItemTouchHelperCallback;
import com.mimacstudy.player.listener.CheckBoxChangeListener;
import com.mimacstudy.player.listener.IBackPressedListener;
import com.mimacstudy.player.listener.IModeChangeListener;
import com.mimacstudy.player.widget.ACTION_MODE;
import com.mimacstudy.player.widget.DividerItemDecoration;
import com.mimacstudy.player.widget.DownloadDialog;
import com.mimacstudy.player.widget.ProgressingDialog;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.data.CourseInfoData;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.ContentsDatabase2;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadData;
import kr.imgtech.lib.zoneplayer.network.NetworkDefine;
import kr.imgtech.lib.zoneplayer.util.StringUtil;

/**
 * 강좌 그룹 화면 Fragment
 * @author kimsanghwan
 * @since 2015. 01. 15.
 */
public class CourseGroupFragment extends Fragment implements BaseInterface
																	, BaseDialogListener
																	, OnClickListener
																	, IntentDataDefine
																	, NetworkDefine
																	, IBackPressedListener
                                                                    , OnStartDragListener
																	{
	
	private static CourseGroupFragment instance;
	
	private ArrayList<CourseInfoData> mListCourseInfo = new ArrayList<>();	// CourseInfoData ArrayList
	private CourseGroupRecyclerAdapter mAdapter;		// 다운로드 리스트 아이템 어댑터(기본)
	private ItemTouchHelper mItemTouchHelper;

	private View mAllContent;							// 전체 뷰
	private RecyclerView mCourseRecyclerView;			// 리스트뷰
	
	private View ivSelect;						// 선택 모드 버튼
	
	private View ivArrowBack;					// 모드 해제 버튼
	private View ivSelectAll;					// 전체 선택 / 선택 해제 버튼
	private View ivDelete;						// 삭제 버튼
	
	private View mActionModeView;				// ActionModeView
	private Animation leftEnter;				// Animation
	private Animation rightEnter;				// ""
	private IModeChangeListener modeChangeListener;	// 모드 변경 리스너
	private ACTION_MODE mode = ACTION_MODE.NORMAL;	// Action Mode - 기본 설정(Normal 모드)
	
	private boolean mViewSelectCheckBox;			// CheckBox 화면 표출 여부
	private boolean mIsAllChecked;					// 전체 선택 및 전체 선택 해제 Flag
	private boolean mNeedAnimation;					// Animation 실행 여부 Flag

	private ContentsDatabase2 mDb;						// 다운로드 아이템 관리 DB 관리자

	private DownloadDialog mDialog;
	private ProgressingDialog mProgressingDialog;
	
	private Bundle mBundle;
	private Intent mIntent;
	
	private String mSiteId;
    
	/**
	 * 생성자
	 */
	public CourseGroupFragment() {
		super();
		
		mDb = ContentsDatabase2.getInstance(getActivity());
		
		mBundle = null;
	}
	
	/**
	 * newInstance
	 * @return	CourseGroupFragment
	 */
	public static CourseGroupFragment newInstance() {
		
		if (instance == null) {
			instance = new CourseGroupFragment();
		} else {
			instance.mBundle = null;
		}
		
		return instance;
	}
	
	/**
	 * newInstance
	 * @param bundle	Bundle
	 * @return	CourseGroupFragment
	 *
	 */
	public static CourseGroupFragment newInstance(Bundle bundle) {
		
		if (instance == null) {
			instance = new CourseGroupFragment();
		}

        instance.mBundle = bundle;
		
		return instance;
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (mBundle != null) {
			mIntent = mBundle.getParcelable(MainActivity.KEY_INTENT);
		} else {
			mIntent = null;
		}
		
		initActionBar();
		initAnimation();
		
		return initView(inflater, null);
	}

	@Override
    public void onResume() {

        super.onResume();
        
        // 목록 아이템 갱신
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
		}
    }

    @Override
    public void onPause() {
        super.onPause();

        new UpdateItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mListCourseInfo);
    }

    @Override
	public void onDestroy() {
		super.onDestroy();
		
		releaseDeleteMode();

		if (mListCourseInfo != null) {
			mListCourseInfo.clear();
		}
		if (mAdapter != null) {
			mAdapter.clear();
		}
	}

	@Override
	public void onDialog(int state, int code, int result) {
		
		switch (state) {
		// 삭제 Dialog
		case DIALOG_DOWNLOAD_DELETE_ITEM:
			new DeleteItemAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			break;

		}
	}

	@Override
	public void onClick(View v) {

		if (v == ivSelect) {
			toggleDeleteMode();
		} else if (v == ivArrowBack) {
			toggleDeleteMode();
		} else if (v == ivDelete) {
			dialogDeleteItem();
		} else if (v == ivSelectAll) {
			mAdapter.setCheckedAll();
		} else if (v == mAllContent) {
			
			Bundle bundle = new Bundle();
			bundle.putParcelable(MainActivity.KEY_INTENT, makeContentIntent(mSiteId, null));
			
			((MainActivity) getActivity()).switchFragment(MainActivity.ID_CONTENTS, bundle);
		}
	}
	
	@Override
	public void onBackPressed() {
		
		if (mode == ACTION_MODE.DELETE) {
			
			releaseDeleteMode();
		} else {

            ((MainActivity)getActivity()).finishApplicationProcess();
		}
	}
	
	/**
	 * View 초기화
	 * @param in Inflater
	 * @param viewGroup ViewGroup
	 * @return	전체 View
	 */
	private View initView(LayoutInflater in, ViewGroup viewGroup) {
		
		View view = in.inflate(R.layout.fragment_course_group, viewGroup);
		
		mCourseRecyclerView = (RecyclerView) view.findViewById(R.id.rv_course_group);
		mCourseRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
		mCourseRecyclerView.setHasFixedSize(true);

		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
		mCourseRecyclerView.setLayoutManager(mLayoutManager);
		mCourseRecyclerView.setItemAnimator(new DefaultItemAnimator());
		
		mAllContent = view.findViewById(R.id.layout_content_all);
		mAllContent.setOnClickListener(this);
		
		// Intent 처리
		if (mIntent != null) {
			
			Uri uri = mIntent.getData();
			if (uri != null) {
				
				mSiteId = uri.getQueryParameter(IntentDataDefine.SITE_ID);
				
				ArrayList<ZoneDownloadData> listData = mDb.getAllFileInfoDetail(mSiteId);
				
				if ((listData != null) && (listData.size()> 0)) {
					
					mListCourseInfo = mDb.getCourseInfoDataBySiteId(mSiteId);
					
					if ((mListCourseInfo != null) && (mListCourseInfo.size() > 0)) {
						// TODO 해당 Site ID의 강좌가 있는 경우
					} else {
						// TODO 해당 Site ID의 강좌가 없는 경우 처리 요망!
					}
					
				} else {
					mListCourseInfo = new ArrayList<>();
				}
				
			}
		}
		
		initList();
		
		return view;
	}
	
	/**
	 * List 및 Adapter 초기화
	 */
	private void initList() {
		
		if (mCourseRecyclerView == null) {
			return;
		}
		
		if (mAdapter != null) {
			mAdapter.clear();
			mAdapter = null;
		}
		mAdapter = new CourseGroupRecyclerAdapter(this, mListCourseInfo, R.layout.row_site);
		modeChangeListener = mAdapter;
		
		mCourseRecyclerView.setAdapter(mAdapter);
		mCourseRecyclerView.addOnItemTouchListener(mAdapter);
		if (rightEnter != null) {
			rightEnter.setAnimationListener(mAdapter);
		}
		
		// 목록이 없으면 편집 선택 버튼 비활성화
		if ((mListCourseInfo != null) && (mListCourseInfo.size() == 0)) {
			ivSelect.setVisibility(View.INVISIBLE);
		}

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mCourseRecyclerView);
	}
	
	/**
	 * ActionBar 초기화
	 */
	@SuppressLint("InflateParams")
	private void initActionBar() {
		
		ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		
		View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_box, null);
		
		ivSelect = rootView.findViewById(R.id.iv_select);
		ivSelect.setOnClickListener(this);
		ivSelect.setContentDescription("선택모드");

		((TextView) rootView.findViewById(R.id.tv_title)).setText("보관함");

		if (actionBar != null) {
			actionBar.setCustomView(rootView);
			actionBar.setDisplayShowCustomEnabled(true);

			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
		}
	}
	
	/**
	 * Transition Animation 초기화
	 */
	private void initAnimation() {
		
		leftEnter = AnimationUtils.loadAnimation(getActivity(), R.anim.left_enter);
		rightEnter = AnimationUtils.loadAnimation(getActivity(), R.anim.right_enter);
	}
	
	/**
	 * 삭제 ActionMode 로 전환
	 */
	@SuppressLint("InflateParams")
	private void setDeleteMode() {
		
		mode = ACTION_MODE.DELETE;

		mActionModeView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_box_actionmode, null);

		ivArrowBack = mActionModeView.findViewById(R.id.iv_arrow_back);
		ivArrowBack.setOnClickListener(this);
		ivSelectAll = mActionModeView.findViewById(R.id.iv_select_all);
		ivSelectAll.setOnClickListener(this);
		ivDelete = mActionModeView.findViewById(R.id.iv_delete);
		ivDelete.setOnClickListener(this);
		
		((MainActivity) getActivity()).setActionModeView(mActionModeView);

		ivArrowBack.startAnimation(leftEnter);
		ivSelectAll.startAnimation(rightEnter);
		ivDelete.startAnimation(rightEnter);
		
		mNeedAnimation = true;
		modeChangeListener.changeMode(mode);
	}
	
	/**
	 * 삭제 ActionMode 해제
	 */
	private void releaseDeleteMode() {
		
		mode = ACTION_MODE.NORMAL;
		
        ((MainActivity) getActivity()).releaseActionModeView(mActionModeView);

        ivArrowBack = null;
        ivDelete = null;
        ivSelectAll = null;
		mActionModeView = null;

		if (modeChangeListener != null) {
			modeChangeListener.changeMode(mode);
		}
	}
	
	/**
	 * ActionBar 를 Normal - Delete 모드로 전환
	 */
	private void toggleDeleteMode() {
		
		if (mode == ACTION_MODE.NORMAL) {
			setDeleteMode();
		} else {
			releaseDeleteMode();
		}
	}
	
	/**
	 * Course Group Fragment Intent 생성
	 * @param siteId	Site ID
	 * @return	Course Group Fragment Intent
	 */
	private Intent makeContentIntent(String siteId, String courseId) {
		
		Intent intent = null;

        String scheme = getResources().getString(R.string.scheme_player);
		String uri;
		
		if (StringUtil.isNotBlank(courseId)) {
			
			uri = String.format((scheme + "://contentslist?"
                            + "siteid=%s"
                            + "&courseid=%s"),
                    siteId,
                    courseId)
					;
		} else {
			
			uri = String.format((scheme + "://contentslist?"
                            + "siteid=%s"),
                    siteId)
					;
		}

		// Intent 설정
		try {
			intent = Intent.parseUri(uri, Intent.URI_INTENT_SCHEME);
			intent.addCategory(Intent.CATEGORY_BROWSABLE);
        	intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());
			intent.setPackage(getActivity().getPackageName());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		return intent;
	}
	
	/**
     * 삭제 Alert 표출
     */
    private void dialogDeleteItem() {

        if (getActivity() == null) {
            return;
        }

        if (mAdapter.getSelectList().contains(true)) {

            mDialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_DELETE_ITEM, 0, "강좌", this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        } else {

            dialogNoSelect();
        }
    }

    /**
     * 선택 강좌 없음 알림 표출
     */
    private void dialogNoSelect() {

        if (getActivity() == null) {
            return;
        }

        String msg = "선택한 강좌가 없습니다.";

        mDialog = DownloadDialog.getInstance(DIALOG_NO_SELECT, 0, msg, this);
        mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
    }
	
    /**
     * ProgressDialog 표출
     */
    private void dialogWaiting() {

        if (getActivity() == null) {
            return;
        }

    	mProgressingDialog = ProgressingDialog.getInstance(0);
    	mProgressingDialog.show(getActivity().getSupportFragmentManager(), ProgressingDialog.DIALOG_TAG);
    }
    
    /**
     * ProgressDialog 해제
     */
    private void dismissWaiting() {

        if (getActivity() == null) {
            return;
        }

    	if (mProgressingDialog != null) {
    		mProgressingDialog.dismiss();
    		mProgressingDialog = null;
		}
    }
    
    /**
     * 해당 강좌 내 모든 파일 및 파일 정보 삭제
     * @param course 강좌 정보
     */
    private void deleteAllFileInfoInCourse(CourseInfoData course) {
    	
		// 강좌에 해당하는 전체 파일 정보 획득
		ArrayList<ZoneDownloadData> listData
		= ContentsDatabase2.getInstance(
                getActivity()).getFileInfoByCourseID(course.siteId, course.courseId);
		
		// 강좌에 해당하는 전체 파일과 파일 정보 삭제
		for (ZoneDownloadData data : listData) {
			
			// 파일 삭제
			ContentFileManager.getInstance(getActivity()).deleteFile(data.filePath);
			// DB 삭제 처리
			ContentsDatabase2.getInstance(getActivity()).deleteFileInfoDetail(data);
	
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    /**
     * 강좌 그룹 RecyclerAdapter
     */
    private class CourseGroupRecyclerAdapter extends RecyclerView.Adapter<ViewHolder>
    										implements OnItemTouchListener, IModeChangeListener
                                            , ItemTouchHelperAdapter
    										, AnimationListener {

        private final OnStartDragListener onStartDragListener;
    	private List<CourseInfoData> list;
    	private int itemLayout;
    	
    	// CheckBox 체크 여부 저장 ArrayList
    	private ArrayList<Boolean> mListCourseSelect = new ArrayList<>();

    	/**
    	 * 생성자
         * @param dragListener OnStartDragListener
    	 * @param li		List<SiteInfoData>
    	 * @param layout 	row Layout
    	 */
		public CourseGroupRecyclerAdapter(OnStartDragListener dragListener, List<CourseInfoData> li, int layout) {
			
			super();

            onStartDragListener = dragListener;
			list = li;
			itemLayout = layout;
			
			initCheckedList(false);
		}

		@Override
		public int getItemCount() {
			
			if (list != null) {
				return list.size();
			} else {
				return 0;
			}
		}

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout, viewGroup, false);

            return new ViewHolder(view);
        }

		@Override
		public void onBindViewHolder(final ViewHolder holder, int position) {
			
			CourseInfoData item = list.get(position);
			
			// 아이콘은 폴더 아이콘으로 일괄 설정
			holder.ivIcon.setImageResource(R.drawable.ic_folder_g);
			holder.tvTitle.setText(item.courseName);
			
			// 삭제 ActionMode 여부에 따라 CheckBox 표출 여부 설정
			if (mViewSelectCheckBox) {

				if (mNeedAnimation) {
					holder.cbSelect.startAnimation(rightEnter);
				}
				holder.cbSelect.setVisibility(View.VISIBLE);
                holder.handleView.setVisibility(View.GONE);
			} else {

				holder.cbSelect.setVisibility(View.GONE);
                holder.handleView.setVisibility(View.VISIBLE);
			}

			holder.cbSelect.setOnCheckedChangeListener(
					new CheckBoxChangeListener(mListCourseSelect, position));
			holder.cbSelect.setChecked(mListCourseSelect.get(position));

            holder.handleView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        onStartDragListener.onStartDrag(holder);
                    }
                    return false;
                }
            });
		}

		@Override
		public boolean onInterceptTouchEvent(RecyclerView arg0, MotionEvent arg1) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onTouchEvent(RecyclerView arg0, MotionEvent arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

		}

		@Override
		public void changeMode(ACTION_MODE mode) {

			if (mode == ACTION_MODE.NORMAL) {
				changeCheckBox(false);
			} else if (mode == ACTION_MODE.DELETE) {
				changeCheckBox(true);
			}
		}

        @Override
        public boolean onItemMove(int fromPosition, int toPosition) {

            // 다운로드 요청 리스트 swap
            Collections.swap(list, fromPosition, toPosition);

            notifyItemMoved(fromPosition, toPosition);

            return true;
        }

        @Override
        public void onItemDismiss(int position) {

        }
		
		@Override
		public void onAnimationStart(Animation animation) {
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			mNeedAnimation = false;
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}
		
		/**
		 * CourseInfoData Item 반환
		 * @param position	Item Position
		 * @return	CourserInfoData
		 */
		public CourseInfoData getItem(int position) {
			
			if (list != null) {
				return list.get(position);
			} else {
				return null;
			}
		}
		
		/**
		 * Item 추가
		 * @param item	추가할 Item
		 */
		public void addItem(CourseInfoData item) {
			
			list.add(item);
			notifyItemInserted(list.size() - 1);

            initCheckedList(false);
		}
		
		/**
		 * Index 설정하고 Item 추가
		 * @param position	추가할 Index
		 * @param item		추가할 Item
		 * @return 추가된 Item Index
		 */
		public int addItem(int position, CourseInfoData item) {
			
			if (position > list.size()) {
				position = list.size();
			}
			list.add(position, item);
			notifyItemInserted(position);
			
			return position;
		}
		
		/**
		 * Item 삭제
		 * @param item	삭제할 Item
		 */
		public void removeItem(CourseInfoData item) {
			
			int position = list.indexOf(item);
			list.remove(position);
			notifyItemRemoved(position);
		}
		
		/**
		 * 전체 Adapter List 초기화
		 */
		public void clear() {
			
			if (list != null) {
				list.clear();
			}
		}

		/**
		 * Delete CheckBox 화면 표출 여부 설정
		 * @param isView	true: 표출, false: otherwise
		 */
		private void changeCheckBox(boolean isView) {

			mViewSelectCheckBox = isView;
			
			notifyDataSetChanged();
		}
		
		/**
		 * CheckBox List 초기화
		 * @param value	초기화할 기본
		 */
		private void initCheckedList(boolean value) {
			
			int count = getItemCount();
            mListCourseSelect.clear();

			for (int i = 0; i < count; i++) {
				
				mListCourseSelect.add(value);
			}
		}
		
		/**
		 * CheckBox 전체 선택 / 해제
		 */
		private void setCheckedAll() {
			
			mIsAllChecked = (! mIsAllChecked);
			
			int count = getItemCount();
			
			for (int i = 0; i < count; i++) {
				
				mListCourseSelect.set(i, mIsAllChecked);
			}
			
			notifyDataSetChanged();
		}

		/**
		 * Select List 반환
		 * @return	Select List
		 */
		private ArrayList<Boolean> getSelectList() {
			
			return mListCourseSelect;
		}
    }

	/**
	 * ViewHolder
	 * @author kimsanghwan
	 * @since  2015. 1. 22.
	 */
	public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {

		private ImageView ivIcon;
		private TextView tvTitle;
        private View handleView;
		private CheckBox cbSelect;

		/**
		 * 생성자
		 * @param itemView	ItemView
		 */
		public ViewHolder(View itemView) {
			
			super(itemView);

			ivIcon = (ImageView) itemView.findViewById(R.id.row_icon);
			tvTitle = (TextView) itemView.findViewById(R.id.row_title);
            handleView = itemView.findViewById(R.id.iv_handle);
            handleView.setVisibility(View.VISIBLE);
			cbSelect = (CheckBox) itemView.findViewById(R.id.cb_select);
			
			itemView.setOnClickListener(this);
		}
		
		@Override
		public void onClick(final View v) {

            if (mViewSelectCheckBox) {
                return;
            }

			Bundle bundle = new Bundle();
			bundle.putParcelable(MainActivity.KEY_INTENT,
					makeContentIntent(mSiteId, mListCourseInfo.get(getLayoutPosition()).courseId));
			
			((MainActivity) getActivity()).switchFragment(MainActivity.ID_CONTENTS, bundle);
		}
	}

    /**
     * 강좌 그룹 정보 업데이트 AsyncTask
     */
    private class UpdateItemAsyncTask extends AsyncTask<ArrayList<CourseInfoData>, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(ArrayList<CourseInfoData>... params) {

            int index = 0;
            ArrayList<CourseInfoData> it = (ArrayList<CourseInfoData>) params[0].clone();

            for (CourseInfoData info:it) {

                info.courseIndex = index++;

                mDb.updateCourseInfo(info);
            }

            return null;
        }
    }

	/**
	 * 선택 강좌 그룹 삭제 AsyncTask
	 * @author kimsanghwan
	 * @since 2014. 10. 23.
	 */
	private class DeleteItemAsyncTask extends AsyncTask<Void, Void, ArrayList<CourseInfoData>> {
	
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			dialogWaiting();
			toggleDeleteMode();	// 삭제 처리가 시작되면 Action Mode 원복
		}
	
		@Override
		protected ArrayList<CourseInfoData> doInBackground(Void... params) {
			
			ArrayList<CourseInfoData> listDelete = new ArrayList<>();
	
			int size = mAdapter.getItemCount();
			ArrayList<Boolean> listSelect = mAdapter.getSelectList();
			
			// 선택된 Item 이 있으면 실행
			if (listSelect.contains(true)) {
				
				// 삭제 일괄 처리
				for (int i = 0; i < size; i++) {
					
					// 선택된 Item 이면 처리 실행
					if (listSelect.get(i)) {
						
		    			// 삭제할 아이템 획득
		    			CourseInfoData data = mAdapter.getItem(i);
		    			// 강의 콘텐츠 및 파일 정보 삭제 처리
		    			deleteAllFileInfoInCourse(data);
						// DB 에서 강좌 그룹 삭제 처리
		    			ContentsDatabase2.getInstance(getActivity()).deleteCourseInfo(data.siteId, data.courseId);
						/*
						 * 삭제 리스트에 추가
						 * AsyncTask 에서는 View 에 변경 이벤트를 전달할 수 없으므로
						 * 별도 삭제 리스트에 추가 후 onPostExecute 에서 처리
						 */
						listDelete.add(data);
						
						// Item 별 처리 delay
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			return listDelete;
		}
	
		@Override
		protected void onPostExecute(ArrayList<CourseInfoData> result) {
			super.onPostExecute(result);

			// Adapter 삭제
			for (CourseInfoData data : result) {
				mAdapter.removeItem(data);
			}
			
			// Select ArrayList 초기화
			mAdapter.initCheckedList(false);

			dismissWaiting();
		}
	}

}
