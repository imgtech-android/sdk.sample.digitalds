package com.mimacstudy.player.fragment;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.Browser;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.mimacstudy.player.MainActivity;
import com.mimacstudy.player.R;
import com.mimacstudy.player.listener.IBackPressedListener;
import com.mimacstudy.player.widget.ACTION_MODE;
import com.mimacstudy.player.widget.DownloadDialog;
import com.mimacstudy.player.widget.ProgressingDialog;

import org.andlib.helpers.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import kr.imgtech.lib.zoneplayer.IMGApplication;
import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.data.IntentDataParser;
import kr.imgtech.lib.zoneplayer.data.IntentDataParserListener;
import kr.imgtech.lib.zoneplayer.gui.download.CancelNotificationReceiver;
import kr.imgtech.lib.zoneplayer.gui.download.ContentFileManager;
import kr.imgtech.lib.zoneplayer.gui.download.ContentsDatabase2;
import kr.imgtech.lib.zoneplayer.gui.download.DownloadInterface;
import kr.imgtech.lib.zoneplayer.gui.download.DownloadService3;
import kr.imgtech.lib.zoneplayer.interfaces.ActiveDownload;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.interfaces.IDownloadItemListener;
import kr.imgtech.lib.zoneplayer.interfaces.IDownloadService;
import kr.imgtech.lib.zoneplayer.interfaces.IDownloadServiceCallback;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadData;
import kr.imgtech.lib.zoneplayer.interfaces.ZoneDownloadReqData;
import kr.imgtech.lib.zoneplayer.network.NetworkDefine;
import kr.imgtech.lib.zoneplayer.network.NetworkManager;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.LibUtil;
import kr.imgtech.lib.zoneplayer.util.StringUtil;
import kr.imgtech.lib.zoneplayer.util.WeakHandler;

/**
 * 다운로드 진행중 화면 Fragment
 * @author kimsanghwan
 * @since 2015.11.25
 */
public class DownloadProgressFragment extends Fragment implements BaseInterface
        , IntentDataDefine
        , NetworkDefine
        , BaseDialogListener
        , OnClickListener
        , DownloadInterface
        , IntentDataParserListener
        , IBackPressedListener
{

    public final static String KEY_INTENT = "intent";

    /*
     * Handler Message Constants 정의
     */
    private final static int MSG_UPDATE_PROGRESS = 1;			// 다운로드 진행 메시지
    private final static int MSG_NOTIFY_STATUS = 2;				// 다운로드 상태 알림 메시지
    private final static int MSG_FILE_INFORMATION = 3;			// 콘텐츠 파일 정보 획득 메시지

    private final static int MSG_START_DOWNLOAD_PROGRESS = 7;	// 다운로드 프로그레스 일괄 시작
    private final static int MSG_END_NOTIFICATION = 9;			// Notification 해제 메시지

    private final static int MSG_START_DOWNLOAD_SERVICE = 11;   // 다운로드 서비스 시작 (1 item)
    private final static int MSG_STOP_DOWNLOAD_SERVICE = 12;    // 다운로드 서비스 정지
    private final static int MSG_START_RESUME_DOWNLOAD = 13;    // 다운로드 재개 시작 (1 item)
    private final static int MSG_ADD_ROW_VIEW = 14;

    /*
     * Message Data Key
     */
    private final static String KEY_MSG_STATUS = "status";		// 다운로드 상태
    private final static String KEY_MSG_MESSAGE = "message";	// 전달 메시지

    private final static long AVAIL_USABLE_SPACE = (100L * 1024L * 1024L);  // 다운로드 가능 유휴 공간 (100MB)

    /*
     * Update Progress Period
     */
    private final static int TIME_UPDATE_PROGRESS = 500;	// Progress 업데이트 주기 0.5초

    private static DownloadProgressFragment instance;	// DownloadService3 에 바인딩되는 static instance

    private static boolean mUpdateProgress;	        // TIME_UPDATE_PROGRESS 마다 동작하기 위한 판단 변수

    private View ivEdit;						// 편집 버튼
    private View ivArrowBack;					// 모드 해제 버튼
    private View ivDelete;						// 전체 삭제 버튼
    private View ivDeleteOk;                    // 삭제 완료 버튼

    private LayoutInflater mInflater;
    private static LinearLayout mLayoutProgress;	// 다운로드 아이템 목록 표출 ViewGroup

    private View mActionModeView;				    // ActionModeView
    private ACTION_MODE mode = ACTION_MODE.NORMAL;	// Action Mode - 기본 설정(Normal 모드)

    private DownloadDialog mDialog;                 // 일반 dialog
    private ProgressingDialog mProgressingDialog;   // 진행 dialog. download pause / resume 실행 시 표시
    private boolean mDownloadServiceRequest;        // download service 명령 요청 후 DownloadService 로부터의 응답 전달 여부

    private Animation mLeftEnter, mRightEnter;

    // 4차 방식
    private static ArrayList<ZoneDownloadReqData> mListDownloadReq = new ArrayList<>();
    private static ArrayList<DownloadItemListener> mListListener = new ArrayList<>();

    private ContentsDatabase2 mDb;

    public static ActiveDownload mActiveDownload;	// 활성 다운로드
    //public static boolean mDownloadStop;          // 다운로드 사용자 정지 여부

    // 다운로드 서비스 Interface
    private static IDownloadService mService;   // DownloadService3 instance
    private static boolean isRegistered;		// 서비스에 등록되었는지 여부

    // 다운로드 프로그레스 전체 Handler
    private static Handler handler;

    // Notification 관련 변수
    private static Notification notification;

    // 전달 번들 및 인텐트
    private Bundle mBundle;
    private Intent mIntent;

    // 네트워크 상태 리시버
    private NetworkStateReceiver mNetworkStateReceiver;
    private static boolean mNetworkOn;      // 네트워크 상태 on 여부

    // 미디어 마운트 상태 리시버
    private MediaMountReceiver mMediaMountReceiver;
    private static boolean mMediaMountError;  // 외부 저장소 탈거 여부

    // onActivityCreated 중복 호출 방지 처리
    private static final int MSG_ACTIVITY_CREATED_OK = 0;
    private static final int PROTECT_TIME = 3000;
    private static boolean mActivityCreated;		// 종료 확인 여부

    // onActivityCreated 중복 호출 방지 처리를 위한 핸들러
    private static Handler mActivityCreatedHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_ACTIVITY_CREATED_OK) {
                mActivityCreated = false;
            }
        }
    };

    // 다운로드 서비스 Connection
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            if (service != null) {

                mService = IDownloadService.Stub.asInterface(service);

                // 서비스 인스턴스 여부 확인
                if (mService != null) {
                    try {
                        isRegistered = mService.registerCallback(mCallback);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }

                // 서비스 인스턴스 확인 및 서비스 콜백에 정상 등록 여부 확인
                if (isRegistered) {
                    handler.sendEmptyMessage(MSG_START_DOWNLOAD_PROGRESS);
                } else {
                    LibUtil.toaster(IMGApplication.getContext(), "다운로드 서비스가 정상 실행되지 않았습니다.");
                }

            }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            if (mService != null) {
                try {
                    mService.unregisterCallback(mCallback);
                    isRegistered = false;
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

    };

    /**
     * IDownloadServiceCallback 구현
     */
    private static IDownloadServiceCallback mCallback = new IDownloadServiceCallback.Stub() {

        @Override
        public void valueChanged(int downloadId, int downloadResult) throws RemoteException {

            Logger.d("completed download-id: " + downloadId);

            int nextDownloadIndex = -1;

            // 활성 다운로드 초기화
            mActiveDownload = null;

            // progress dialog 표출 시 dismiss
            instance.dismissWaiting();

            // 무조건 외장 SD 카드 확인
            if (!ContentFileManager.getInstance(IMGApplication.getContext()).availExtSDCard()) {
                ConfigurationManager.setUseExtSDCard(IMGApplication.getContext(), false);
                ContentFileManager.getInstance(IMGApplication.getContext()).setUseExtSDCard(false);
            }

            // 다운로드 상태가 정지이면 이후 진행하지 않음
            if (downloadResult == RESULT_STOP) {
                Logger.d("paused download");
                return;
            }

            // 미디어 마운트 에러이면 이후 진행하지 않음
            if (mMediaMountError) {
                handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);
                return;
            }

            if (downloadResult == RESULT_SUCCESS) {

                int index = instance.getIndexByDownloadId(downloadId);

                // 다운로드 요청 데이터 획득
                ZoneDownloadReqData reqData = mListDownloadReq.get(index);

                // 파일 크기 설정
                double size;
                // 파일 크기 설정
                if (StringUtil.isNotBlank(reqData.fileSize)) {
                    try {
                        size = Double.parseDouble(reqData.fileSize);
                    } catch (NumberFormatException e) {
                        size = 0.0f;
                    }
                } else {
                    size = 0.0f;
                }

                View view = instance.mLayoutProgress.getChildAt(index);
                TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);
                TextView tvRate = (TextView) view.findViewById(R.id.tv_rate);
                ProgressBar pbProgress = (ProgressBar) view.findViewById(R.id.pb_progress);

                ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);
                ImageView ivPlay = (ImageView) view.findViewById(R.id.iv_play);

                tvProgress.setText(
                        String.format("%s MB", instance.getFormatRateDouble((size / (double) 1024000))));

                tvRate.setVisibility(View.GONE);
                pbProgress.setVisibility(View.GONE);

                ivPauseResume.setVisibility(View.GONE);
                if (instance.mode == ACTION_MODE.NORMAL) {
                    ivPlay.setVisibility(View.VISIBLE);
                }
            }

            // download-id 로 list index 조회
            int start = instance.getIndexByDownloadId(downloadId);

            // 네트워크 오류 상태
/*

            if (!mNetworkOn) {

                handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);

                for (int i = start; i < mListDownloadReq.size(); i++) {

                    mListDownloadReq.get(i).downloadResult = RESULT_FAIL;
                    mListDownloadReq.get(i).downloadMsg = "네트워크 실패";
                }

                instance.mAdapter.notifyDataSetChanged();
                return;
            }
*/

            if (start < 0) {
                Logger.d("start index < 0");
                return;

            // 다운로드 완료
            } else if ((start + 1) > (mListDownloadReq.size() - 1)){

                handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);
                Logger.d("start index >= mListDownloadReq.size()");
                return;
            }

            // 현재 전달받은 완료된 다운로드 id 이후 아이템 중 다운로드할 아이템 조회
            for (int i = start + 1; i < mListDownloadReq.size(); i++) {

                // 다운로드 성공이 아닌 아이템 조회
                if (mListDownloadReq.get(i).downloadResult < RESULT_SUCCESS) {
                    nextDownloadIndex = i;
                    break;
                }
            }

            // 2. 다음 다운로드할 아이템이 있으면 실행
            if (nextDownloadIndex > -1) {

                Logger.d("next download index: " + nextDownloadIndex);

/*

				mActiveDownload = new ActiveDownload(
                        mListDownloadReq.get(nextDownloadIndex),
						mListListener.get(nextDownloadIndex));
*/

                handler.removeMessages(MSG_START_RESUME_DOWNLOAD);
                handler.removeMessages(MSG_START_DOWNLOAD_SERVICE);

                // 다운로드 받은 사이즈가 0 이상
                if (mListDownloadReq.get(nextDownloadIndex).downloadSize > 0) {

                    // Message 전달
                    // handler.sendEmptyMessageDelayed(MSG_START_RESUME_DOWNLOAD, 1000);
                    Message message = handler.obtainMessage(MSG_START_RESUME_DOWNLOAD, nextDownloadIndex, -1);
                    handler.sendMessageDelayed(message, 1000);	// Message 전달
                } else {

                    // Message 전달
                    // handler.sendEmptyMessageDelayed(MSG_START_DOWNLOAD_SERVICE, 1000);
                    Message message = handler.obtainMessage(MSG_START_DOWNLOAD_SERVICE, nextDownloadIndex, -1);
                    handler.sendMessageDelayed(message, 1000);	// Message 전달
                }

            }

        }
    };

    /**
     * 생성자
     */
    public DownloadProgressFragment() {

        super();

        mDb = ContentsDatabase2.getInstance(getActivity());
    }

    /**
     * newInstance
     * @return ZoneDownloadProgressBaseFragment
     */
    public static DownloadProgressFragment newInstance() {

        if (instance == null) {
            instance = new DownloadProgressFragment();
        }

        // bundle 명시적 초기화 처리.
        if (instance.mBundle != null) {
            instance.mBundle = null;
        }
		
		/*
		 * handler 가 null 인 상황 발생
		 * 명시적으로 instance 의 null 여부 확인에서 분리 후 handler 의 null 여부 확인 처리
		 * @date 2014.12.24
		 */
        if (handler == null) {
            handler = new DownloadProgressHandler(instance);
        }

        return instance;
    }

    /**
     * 생성자
     * @param bundle Bundle
     * @return	DownloadProgressFragment
     */
    public static DownloadProgressFragment newInstance(Bundle bundle) {

        if (instance == null) {
            instance = new DownloadProgressFragment();
        }
		
		/*
		 * handler 가 null 인 상황 발생
		 * 명시적으로 instance 의 null 여부 확인에서 분리 후 handler 의 null 여부 확인 처리
		 * @date 2014.12.24
		 */
        if (handler == null) {
            handler = new DownloadProgressHandler(instance);
        }

        if (instance.mBundle != null) {
            instance.mBundle = null;
        }

        instance.mBundle = bundle;

        return instance;
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inf, ViewGroup container, Bundle savedInstanceState) {

        mInflater = inf;
        return inf.inflate(R.layout.fragment_download_progress, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initActionBar();
        initAnimation();
        initList(view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // 1. 현재 네트워크 및 이동 통신망 허용 여부 확인
        // WiFi가 아니고 / 이동 통신망 허용이 아니면
        // DownloadIntent 가 반드시 존재하고
        // 현재 다운로드 중인 아이템이 없으면
        if ((! LibUtil.isWifi(getContext()))
                && (ConfigurationManager.getWiFiOnly(getContext()))
                && (instance.mBundle != null)
                ) {

            dialogNetworkAlert();
        }

        mNetworkOn = true;

        // Bundle Intent 획득
        if (mBundle != null) {
            mIntent = mBundle.getParcelable(KEY_INTENT);
        } else {
            mIntent = null;
        }

        // 2. 다운로드 서비스 바인드
        startServiceBind();

        // 3. 네트워크 상태 리시버 시작
        // startNetworkStateReceiver();

        startMediaMountReceiver();

        //startActivityCreatedHandler();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // 1. 다운로드 서비스 언바인드
        unbindService();

        // 2. 현재 리스트 상태 저장
        saveLastDownloadReqStatus();
        //handler.sendEmptyMessage(MSG_SAVE_LAST_STATUS);

        // 3. 데이터 초기화
        initData();

        // 4. action-mode 초기화
        releaseDeleteMode();

        // 4. 네트워크 상태 리시버 종료
        // stopNetworkStateReceiver();

        stopMediaMountReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();

        // 무조건 외장 SD 카드 확인
        if (!ContentFileManager.getInstance(getContext()).availExtSDCard()) {
            ConfigurationManager.setUseExtSDCard(getContext(), false);
            ContentFileManager.getInstance(getContext()).setUseExtSDCard(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // 무조건 외장 SD 카드 확인
        if (! ContentFileManager.getInstance(IMGApplication.getContext()).availExtSDCard()) {
            ConfigurationManager.setUseExtSDCard(IMGApplication.getContext(), false);
            ContentFileManager.getInstance(IMGApplication.getContext()).setUseExtSDCard(false);
        }
    }

    @Override
    public void onBackPressed() {

        if (mode == ACTION_MODE.DELETE) {

            //releaseDeleteMode();
            toggleDeleteMode();
        } else {

            ((MainActivity)getActivity()).finishApplicationProcess();
        }
    }

    @Override
    public void onDialog(int state, int code, int result) {

        switch (state) {

            // 다운로드 전체 취소
            case DIALOG_DOWNLOAD_DELETE_ALL:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {
                    setDeleteAll();
                }
                break;

            // 다운로드 아이템 취소
            case DIALOG_DOWNLOAD_CANCEL_ITEM:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {

                    try {
                        mService.cancelDownload(code);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    //setCancelDownloadView(code);
                }
                break;

            // 최초 화면 생성 시 네트워크 경고
            case DIALOG_NETWORK_WARNING:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {

                    // 3. 다운로드 서비스 바인드
                    startServiceBind();
                }
                break;

            // 업데이트 확인 메시지
            case DIALOG_UPDATE_YESNO:

                switch (result) {
                    case DownloadDialog.DIALOG_CHOICE_POSITIVE:	// 업데이트
                        updateStore();
                        break;

                    default:	// 종료
                        getActivity().finish();
                        break;
                }
                break;

            // 활성 다운로드 삭제 확인 메시지
            case DIALOG_CONFIRM_DELETE_DOWNLOAD:

                switch (result) {

                    case DownloadDialog.DIALOG_CHOICE_POSITIVE: // 삭제

                        // 다운로드 정지
                        handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);

                        // 파일 삭제
                        ContentFileManager.getInstance(getContext()).deleteFile(
                                mActiveDownload.DOWNLOAD_REQ_DATA.filePath);

                        // 리스트 아이템 삭제
                        removeDownloadItem(code);

                        // 활성 다운로드 삭제
                        mActiveDownload = null;
                        break;
                }
                break;

            // 활성 다운로드 포함 전체 삭제 확인 메시지
            case DIALOG_CONFIRM_DELETE_ALL:

                switch (result) {

                    case DownloadDialog.DIALOG_CHOICE_POSITIVE: // 삭제

                        // 다운로드 정지
                        handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);

                        // 전체 삭제
                        setDeleteAll();

                        // 활성 다운로드 삭제
                        mActiveDownload = null;
                        break;
                }
                break;

            // 네트워크 없음
            case DIALOG_NO_NETWORK:

                if (result == DownloadDialog.DIALOG_CHOICE_POSITIVE) {

                    // 네트워크 미연결
                    if (! LibUtil.isNetworkAvailable(getActivity())) {

                        // 네트워크 없음 알림
                        dialogNoNetwork(code);
                        return;
                    }

                    Message message = handler.obtainMessage(MSG_START_RESUME_DOWNLOAD, code, -1);
                    handler.sendMessageDelayed(message, 1000);	// Message 전달
                }
                break;

        }
    }

    @Override
    public void onParseFail(int errorCode) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onParseMessage(int error, String message) {
        // TODO Auto-generated method stub
    }

    @SuppressLint("InflateParams")
    @Override
    public void onClick(View v) {

        if (v == ivEdit) {

            toggleDeleteMode();

        } else if (v == ivArrowBack) {

            toggleDeleteMode();

        } else if (v == ivDeleteOk) {

            toggleDeleteMode();

        } else if (v == ivDelete) {

            dialogDeleteAll();
        }
    }

    /**
     * onViewCreated 중복 호출 방지 handler 시작
     */
    private static void startActivityCreatedHandler() {

        mActivityCreated = true;
        mActivityCreatedHandler.sendEmptyMessageDelayed(MSG_ACTIVITY_CREATED_OK, PROTECT_TIME);
    }

    /**
     * ActionBar 초기화
     */
    @SuppressLint("InflateParams")
    private void initActionBar() {

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_progress, null);

        ((TextView) rootView.findViewById(R.id.tv_title)).setText("다운로드");

        ivEdit = rootView.findViewById(R.id.iv_select);
        ivEdit.setOnClickListener(this);
        // ivRemoveAll = rootView.findViewById(R.id.iv_cancel_all);
        // ivRemoveAll.setOnClickListener(this);

        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setCustomView(rootView);
            actionBar.setDisplayShowCustomEnabled(true);

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    /**
     * Animation 초기화
     */
    private void initAnimation() {

        mLeftEnter = AnimationUtils.loadAnimation(getActivity(), R.anim.left_enter);
        mRightEnter = AnimationUtils.loadAnimation(getActivity(), R.anim.right_enter);
    }

    /**
     * List 및 Adapter 초기화
     */
    private void initList(View view) {

        mLayoutProgress = (LinearLayout) view.findViewById(R.id.layout_progress);

        restoreList();
        //new RestoreAsyncTask().execute();
    }

    /**
     * 다운로드 목록을 DB와 비교해서 복원
     */
    private void restoreList() {

        ArrayList<ZoneDownloadReqData> dbList = mDb.getDownloadReqInfo();

        //mListDownloadReq.clear();
        //mListListener.clear();

        if (mLayoutProgress != null) {

            mLayoutProgress.removeAllViews();
        }

        if ((mListDownloadReq.size() == 0) && (dbList.size() > 0)) {

            for (ZoneDownloadReqData reqData : dbList) {

                // 다운로드 아이템 리스너 생성
                DownloadItemListener listener = new DownloadItemListener();

                // root-view 에 추가
                addDownloadItem(reqData, listener, true);
            }
        } else {

            for (int i = 0; i < mListDownloadReq.size(); i++) {

                ZoneDownloadReqData reqData = mListDownloadReq.get(i);

                DownloadItemListener listener;
                if (mListListener.size() > i) {
                    listener = mListListener.get(i);
                } else {
                    listener = new DownloadItemListener();
                }

                addDownloadItem(reqData, listener, false);
            }
        }
    }

    /**
     * 데이터 초기화
     */
    private void initData() {

        mDownloadServiceRequest = false;
        mMediaMountError = false;
    }

    /**
     * 삭제 ActionMode 로 전환
     */
    @SuppressLint("InflateParams")
    private void setDeleteMode() {

        mode = ACTION_MODE.DELETE;

        mActionModeView = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_progress_actionmode, null);

        ivArrowBack = mActionModeView.findViewById(R.id.iv_arrow_back);
        ivArrowBack.setOnClickListener(this);
        ivDelete = mActionModeView.findViewById(R.id.btn_delete);
        ivDelete.setOnClickListener(this);
        ivDeleteOk = mActionModeView.findViewById(R.id.btn_ok);
        ivDeleteOk.setOnClickListener(this);

        ((MainActivity) getActivity()).setActionModeView(mActionModeView);

        ivArrowBack.startAnimation(mLeftEnter);
        ivDelete.startAnimation(mRightEnter);
        ivDeleteOk.startAnimation(mRightEnter);
    }

    /**
     * 리스트 모드 변경
     * @param mode 변경할 모드
     */
    private void changeListMode(boolean mode) {

        int count = mLayoutProgress.getChildCount();
        for (int i = 0; i < count; i++) {

            View view = mLayoutProgress.getChildAt(i);
            ZoneDownloadReqData data = mListDownloadReq.get(i);

            ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);
            ImageView ivPlay = (ImageView) view.findViewById(R.id.iv_play);
            ImageView ivRemove = (ImageView) view.findViewById(R.id.iv_remove);

            // 편집 모드
            if (mode) {

                ivPauseResume.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                ivRemove.setVisibility(View.VISIBLE);
            // 일반 모드
            } else {

                if (data.downloadResult == RESULT_SUCCESS) {
                    ivPlay.setVisibility(View.VISIBLE);
                } else {
                    ivPauseResume.setVisibility(View.VISIBLE);
                }

                ivRemove.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 삭제 ActionMode 해제
     */
    private void releaseDeleteMode() {

        mode = ACTION_MODE.NORMAL;

        ((MainActivity) getActivity()).releaseActionModeView(mActionModeView);

        ivArrowBack = null;
        ivDelete = null;
        ivDeleteOk = null;
        mActionModeView = null;
    }

    /**
     * ActionBar 를 Normal - Delete 모드로 전환
     */
    private void toggleDeleteMode() {

        if (mode == ACTION_MODE.NORMAL) {

            setDeleteMode();
            //mAdapter.changeMode(mode);
            changeListMode(true);
        } else {

            releaseDeleteMode();
            //mAdapter.changeMode(mode);
            changeListMode(false);
        }
    }

    /**
     * DownloadService 시작 및 바인드
     * Fragment 가 생성되면 반드시 호출 요망
     */
    private void startServiceBind() {

        IMGApplication.getContext().startService(new Intent(IMGApplication.getContext(), DownloadService3.class));
        IMGApplication.getContext().bindService(new Intent(IMGApplication.getContext(), DownloadService3.class),
                mConnection, Context.BIND_AUTO_CREATE);
    }

    /**
     * DownloadService 언바인드 및 정지
     * Fragment 가 해제되면 반드시 호출 요망
     */
    private void unbindService() {

        if ((mConnection != null) && (mService != null)) {

            IMGApplication.getContext().unbindService(mConnection);
            // 다운로드 서비스가 진행중일 수 있으므로 강제 정지시키지 않음
            //IMGApplication.getAppContext().stopService(new Intent(IMGApplication.getAppContext(), DownloadService3.class));
        }
    }

    /**
     * 네트워크 상태 변화 수신 리시버 시작
     */
    private void startNetworkStateReceiver() {

        if (IMGApplication.getContext() == null) {
            return;
        }

        if (mNetworkStateReceiver != null) {
            return;
        }

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.RSSI_CHANGED_ACTION);

        mNetworkStateReceiver = new NetworkStateReceiver();
        IMGApplication.getContext().registerReceiver(mNetworkStateReceiver, filter);
    }

    /**
     * 네트워크 상태 변화 수신 리시버 정지
     */
    private void stopNetworkStateReceiver() {

        if (IMGApplication.getContext() == null) {
            return;
        }

        if (mNetworkStateReceiver != null) {
            IMGApplication.getContext().unregisterReceiver(mNetworkStateReceiver);
            mNetworkStateReceiver = null;
        }
    }

    /**
     * 미디어 마운트 리시버 시작
     */
    private void startMediaMountReceiver() {

        if (IMGApplication.getContext() == null) {
            return;
        }

        // 외부 저장소 사용 상태가 아님
        if (!ConfigurationManager.getUseExtSDCard(IMGApplication.getContext())) {
            return;
        }

        if (mMediaMountReceiver != null) {
            return;
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(Intent.ACTION_MEDIA_EJECT);
        //filter.addAction(Intent.ACTION_UMS_CONNECTED);
        //filter.addAction(Intent.ACTION_UMS_DISCONNECTED);
        //filter.addAction(Intent.ACTION_MEDIA_SCANNER_STARTED);
        //filter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
        filter.addDataScheme("file");

        mMediaMountReceiver = new MediaMountReceiver();
        IMGApplication.getContext().registerReceiver(mMediaMountReceiver, filter);
    }

    /**
     * 미디어 마운트 리시버 정지
     */
    private void stopMediaMountReceiver() {

        if (IMGApplication.getContext() == null) {
            return;
        }

        // 외부 저장소 사용 상태가 아님
        if (!ConfigurationManager.getUseExtSDCard(IMGApplication.getContext())) {
            return;
        }

        if (mMediaMountReceiver != null) {
            IMGApplication.getContext().unregisterReceiver(mMediaMountReceiver);
            mMediaMountReceiver = null;
        }
    }

    /**
     * 전달받은 다운로드 Intent 처리 시작
     * Intent 가 있는 경우에만 진행
     */
    private void startDownloadProgress() {

        new DataProcessingTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * intent data 파싱
     * @return  파싱 완료된 intent data 및 결과
     */
    private IntentDataParser.IntentDataParserResult2 initIntentDataList3() {

        Intent intent = mIntent;

        IntentDataParser parser = new IntentDataParser(getContext(), intent, this);	// IntentDataParser 생성 및 Intent 전달

        return parser.parseDownloadReqDataList();
    }

    /**
     * double 데이터를 소수점 x자리 문자열로 변환
     * @param size	소수점 자리 수
     * @return	변환된 문자열
     */
    private String getFormatRateDouble(double size) {
        return String.format(java.util.Locale.US, "%.1f", size);
    }

    /**
     * Time 을 날짜-시간 형식 문자로 변환
     * @param date	Time (초 단위)
     * @return	날짜-시간 형식 문자열
     */
    @SuppressLint("DefaultLocale")
    private String getFormatDateTime(long date) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date * 1000);

        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);

        return String.format("%04d.%02d.%02d %02d:%02d:%02d", year, mon + 1, day, hour, min, sec);
    }

    /**
     * Time 을 날짜 형식 문자로 반환
     * @param date	Time (초 단위)
     * @return 날짜 형식 문자열
     */
    @SuppressLint("DefaultLocale")
    private String getFormatDate(long date) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date * 1000);

        int year = cal.get(Calendar.YEAR);
        int mon = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        return String.format("%04d.%02d.%02d", year, mon + 1, day);
    }

    /**
     * 다운로드 아이템 추가
     * @param reqData   ZoneDownloadReqData
     * @param listener  DownloadItemListener
     * @param add 다운로드 array list 추가 여부
     */
    @SuppressLint("InflateParams")
    private synchronized void addDownloadItem(ZoneDownloadReqData reqData, DownloadItemListener listener, boolean add) {

        // row 를 이루는 view-group inflate
        View view = mInflater.inflate(R.layout.row_download_progress, null);

        // child View 초기화
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvCreateTime = (TextView) view.findViewById(R.id.tv_create_time);
        TextView tvTerm = (TextView) view.findViewById(R.id.tv_term);
        TextView tvFileInfo = (TextView) view.findViewById(R.id.tv_file_info);
        ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);
        ImageView ivPlay = (ImageView) view.findViewById(R.id.iv_play);
        ImageView ivRemove= (ImageView) view.findViewById(R.id.iv_remove);

        TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);
        TextView tvRate = (TextView) view.findViewById(R.id.tv_rate);
        ProgressBar pbProgress = (ProgressBar) view.findViewById(R.id.pb_progress);
        pbProgress.setMax(100);

        // 타이틀
        tvTitle.setText(reqData.fileTitle);

        // 다운로드 정지/재개 view 클릭 리스너 설정
        ivPauseResume.setOnClickListener(
                new DownloadStopResumeListener(reqData.downloadId, ivPauseResume));

        // 다운로드 재생 view 클릭 리스너 실행
        ivPlay.setOnClickListener(new PlayContentClickListener(reqData));

        // 아이템 삭제 리스너 설정
        ivRemove.setOnClickListener(
                new RemoveItemListener(reqData.downloadId));

        switch (reqData.downloadResult) {

            case RESULT_FAIL:
            case RESULT_STOP:
            case RESULT_PROCESS:

                // 파일 크기 설정
                double size;
                int percent;
                String currSize;

                // 파일 크기 설정
                if (StringUtil.isNotBlank(reqData.fileSize)) {
                    try {
                        size = Double.parseDouble(reqData.fileSize);
                    } catch (NumberFormatException e) {
                        size = 0.0f;
                    }
                } else {
                    size = 0.0f;
                }

                // 다운로드 rate 계산
                percent = (int) (((double) reqData.downloadSize / size) * 100);

                // 받은 사이즈 / 총 사이즈 문자열 구성
                if (size > 0) {
                    currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                            + " MB/"
                            + getFormatRateDouble(size / (double) 1024000)
                            + " MB";


                    tvProgress.setText(currSize);
                }

                if (reqData.createTime > 0) {
                    tvCreateTime.setText(getFormatDate(reqData.createTime));
                }

                if ((reqData.startTime > 0) && (reqData.endTime > 0)) {
                    tvTerm.setText(
                            String.format("%s - %s", getFormatDate(reqData.startTime), getFormatDate(reqData.endTime)));
                } else {
                    tvTerm.setVisibility(View.INVISIBLE);
                }

                if (StringUtil.isNotBlank(reqData.fileInfo)) {
                    tvFileInfo.setText(reqData.fileInfo);
                } else {
                    tvFileInfo.setVisibility(View.INVISIBLE);
                }

                // 다운로드 실패 - 실패 메시지 표출
                if (reqData.downloadResult == RESULT_FAIL) {
                    tvRate.setText(reqData.downloadMsg);

                // 다운로드 정지 - rate % 표출
                } else {
                    tvRate.setText(percent + "%");
                }

                pbProgress.setProgress(percent);
                break;

            case RESULT_SUCCESS:

                // 파일 크기 설정
                if (StringUtil.isNotBlank(reqData.fileSize)) {
                    try {
                        size = Double.parseDouble(reqData.fileSize);
                    } catch (NumberFormatException e) {
                        size = 0.0f;
                    }
                } else {
                    size = 0.0f;
                }

                tvCreateTime.setText(getFormatDate(reqData.createTime));

                if ((reqData.startTime > 0) && (reqData.endTime > 0)) {
                    tvFileInfo.setText(
                            String.format("%s - %s",
                                    getFormatDate(reqData.startTime), getFormatDate(reqData.endTime)));
                } else {
                    tvFileInfo.setVisibility(View.INVISIBLE);
                }

                tvProgress.setText(
                        String.format("%s MB", getFormatRateDouble((size / (double) 1024000))));

                tvRate.setVisibility(View.GONE);
                pbProgress.setVisibility(View.GONE);

                ivPauseResume.setVisibility(View.GONE);
                ivPlay.setVisibility(View.VISIBLE);
                break;

        }

        // 활성 다운로드 존재
        if (mActiveDownload != null) {
            if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadId == reqData.downloadId) {

                ivPauseResume.setImageResource(R.drawable.ic_pause_circle_g);

            } else {

                ivPauseResume.setImageResource(R.drawable.ic_get_g);

            }
            // 활성 다운로드 없음 - 모든 아이템이 다운로드 버튼으로만 표출
        } else {

            ivPauseResume.setImageResource(R.drawable.ic_get_g);
        }

        if (add) {
            mListDownloadReq.add(reqData);
            mListListener.add(listener);
        }

        listener.setView(view);

        // List 에 row 추가
        //mLayoutProgress.addView(view);
        Message message = handler.obtainMessage(MSG_ADD_ROW_VIEW, 0, 0, view);
        handler.sendMessage(message);
    }

    @SuppressLint("InflateParams")
    private synchronized void removeDownloadItem(int index) {

        mListDownloadReq.remove(index);
        mListListener.remove(index);

        if (mLayoutProgress != null) {
            mLayoutProgress.removeViewAt(index);
        }
    }

    /**
     * 다운로드 진행 단계 수치 업데이트
     * @param view		업데이트할 rowView
     */
    private void progressItemView(View view, int index) {

        if (mListDownloadReq.size() > index) {

            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            // 다운로드 성공 및 실패 시 처리하지 않음
            if (reqData.downloadResult == RESULT_SUCCESS
                    || reqData.downloadResult == RESULT_FAIL) {
                return;
            }

            TextView tvCreateTime = (TextView) view.findViewById(R.id.tv_create_time);
            TextView tvTerm = (TextView) view.findViewById(R.id.tv_term);
            TextView tvFileInfo = (TextView) view.findViewById(R.id.tv_file_info);

            TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);
            TextView tvRate = (TextView) view.findViewById(R.id.tv_rate);
            ProgressBar pbProgress = (ProgressBar) view.findViewById(R.id.pb_progress);

            double size;
            // 파일 크기 설정
            if (StringUtil.isNotBlank(reqData.fileSize)) {
                try {
                    size = Double.parseDouble(reqData.fileSize);
                } catch (NumberFormatException e) {
                    size = 0.0f;
                }
            } else {
                size = 0.0f;
            }

            // 다운로드 rate 계산
            int percent = (int) (((double) reqData.downloadSize / size) * 100);

            String currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                    + " MB/"
                    + getFormatRateDouble(size / (double) 1024000)
                    + " MB";

            tvCreateTime.setText(getFormatDate(reqData.createTime));

            if ((reqData.startTime > 0) && (reqData.endTime > 0)) {
                tvTerm.setText(
                        String.format("%s - %s", getFormatDate(reqData.startTime), getFormatDate(reqData.endTime)));
            } else {
                tvTerm.setVisibility(View.INVISIBLE);
            }

            if (StringUtil.isNotBlank(reqData.fileInfo)) {
                tvFileInfo.setText(reqData.fileInfo);
            } else {
                tvFileInfo.setVisibility(View.INVISIBLE);
            }

            tvProgress.setText(currSize);
            tvRate.setText(String.format("%d%%", percent));

            pbProgress.setProgress(percent);

            updateProgressNotification(currSize, percent);
        }
    }

    /**
     * 다운로드 아이템 상태 설정
     * 다운로드 완료 / 실패 / 취소의 3가지 상태 설정
     * @param view		설정을 표출할 rowView
     * @param index		설정할 다운로드 아이템의 인덱스
     */
    private void statusItemView(View view, int index) {

        if (mListDownloadReq.size() > index) {

            // 다운로드 요청 데이터 획득
            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            if (!mMediaMountError) {
                // 외부 저장소 사용
                if (ConfigurationManager.getUseExtSDCard(getContext())) {
                    reqData.downloadLocation = LOCATION_EXT1;
                    Logger.d("외부 저장소 사용");
                    // 내부 저장소 사용
                } else {
                    reqData.downloadLocation = LOCATION_INNER;
                    Logger.d("내부 저장소 사용");
                }
            }

            Logger.d("index: " + index + ", result: " + reqData.downloadResult);

            TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);
            TextView tvRate = (TextView) view.findViewById(R.id.tv_rate);
            ProgressBar pbProgress = (ProgressBar) view.findViewById(R.id.pb_progress);

            ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);
            ImageView ivPlay = (ImageView) view.findViewById(R.id.iv_play);

            switch (reqData.downloadResult) {

                // 다운로드 성공
                case RESULT_SUCCESS:

                    // 파일 크기 설정
                    double size;
                    // 파일 크기 설정
                    if (StringUtil.isNotBlank(reqData.fileSize)) {
                        try {
                            size = Double.parseDouble(reqData.fileSize);
                        } catch (NumberFormatException e) {
                            size = 0.0f;
                        }
                    } else {
                        size = 0.0f;
                    }

                    tvProgress.setText(
                            String.format("%s MB", getFormatRateDouble((size / (double) 1024000))));

                    tvRate.setVisibility(View.GONE);
                    pbProgress.setVisibility(View.GONE);

                    ivPauseResume.setVisibility(View.GONE);
                    if (mode == ACTION_MODE.NORMAL) {
                        ivPlay.setVisibility(View.VISIBLE);
                    }
                    break;

                // 다운로드 정지
                case RESULT_STOP:

                    // 파일 크기 설정
                    if (StringUtil.isNotBlank(reqData.fileSize)) {
                        try {
                            size = Double.parseDouble(reqData.fileSize);
                        } catch (NumberFormatException e) {
                            size = 0.0f;
                        }
                    } else {
                        size = 0.0f;
                    }

                    // 다운로드 rate 계산
                    int percent = (int) (((double) reqData.downloadSize / size) * 100);

                    String currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                            + " MB/"
                            + getFormatRateDouble(size / (double) 1024000)
                            + " MB";

                    tvProgress.setText(currSize);
                    tvRate.setText(String.format("%d%%", percent));

                    pbProgress.setProgress(percent);

                    ivPauseResume.setImageResource(R.drawable.ic_get_g);
                    break;

                // 다운로드 실패
                case RESULT_FAIL:

                    // 파일 크기 설정
                    if (StringUtil.isNotBlank(reqData.fileSize)) {
                        try {
                            size = Double.parseDouble(reqData.fileSize);
                        } catch (NumberFormatException e) {
                            size = 0.0f;
                        }
                    } else {
                        size = 0.0f;
                    }

                    // 다운로드 rate 계산
                    percent = (int) (((double) reqData.downloadSize / size) * 100);

                    currSize = getFormatRateDouble((double) reqData.downloadSize / (double) 1024000)
                            + " MB/"
                            + getFormatRateDouble(size / (double) 1024000)
                            + " MB";

                    tvProgress.setText(currSize);
                    tvRate.setText(String.format("%d%%", percent));
                    pbProgress.setProgress(percent);

                    ivPauseResume.setImageResource(R.drawable.ic_get_g);
                    tvRate.setText(reqData.downloadMsg);
                    break;

                // 다운로드 진행 중
                case RESULT_PROCESS:

                    ivPauseResume.setImageResource(R.drawable.ic_pause_circle_g);
                    break;


            }
        }
    }

    private void infoItemView(View view, int index) {

        if (mListDownloadReq.size() > index) {

            // 다운로드 요청 데이터 획득
            ZoneDownloadReqData reqData = mListDownloadReq.get(index);

            // item-view 의 상세 view 획득
            ImageView ivPauseResume = (ImageView) view.findViewById(R.id.iv_pause_resume);

            TextView tvCreateTime = (TextView) view.findViewById(R.id.tv_create_time);
            TextView tvTerm = (TextView) view.findViewById(R.id.tv_term);
            TextView tvFileInfo = (TextView) view.findViewById(R.id.tv_file_info);

            TextView tvProgress = (TextView) view.findViewById(R.id.tv_progress);

            tvCreateTime.setText(getFormatDate(reqData.createTime));

            if ((reqData.startTime > 0) && (reqData.endTime > 0)) {
                tvTerm.setText(
                        String.format("%s - %s", getFormatDate(reqData.startTime), getFormatDate(reqData.endTime)));
            } else {
                tvTerm.setVisibility(View.INVISIBLE);
            }

            if (StringUtil.isNotBlank(reqData.fileInfo)) {
                tvFileInfo.setText(reqData.fileInfo);
            } else {
                tvFileInfo.setVisibility(View.INVISIBLE);
            }

            switch (reqData.downloadResult) {

                case RESULT_FAIL:
                case RESULT_STOP:
                    ivPauseResume.setImageResource(R.drawable.ic_get_g);
                    break;

                // 다운로드 진행 중
                case RESULT_PROCESS:
                    ivPauseResume.setImageResource(R.drawable.ic_pause_circle_g);
                    break;

                case RESULT_SUCCESS:

                    // 파일 크기 설정
                    double size;
                    // 파일 크기 설정
                    if (StringUtil.isNotBlank(reqData.fileSize)) {
                        try {
                            size = Double.parseDouble(reqData.fileSize);
                        } catch (NumberFormatException e) {
                            size = 0.0f;
                        }
                    } else {
                        size = 0.0f;
                    }

                    tvProgress.setText(
                            String.format("%s MB", getFormatRateDouble((size / (double) 1024000))));
                    break;
            }
        }
    }

    /**
     * 다운로드 데이터를 재생 데이터 형식으로 변환하여 반환
     * @param data	ZoneDownloadData
     * @param playUrl 미디어 재생 URL
     * @return	ZonePlayerData String
     */
    private String convertZonePlayerData(ZoneDownloadData data, String playUrl) {
		
		/*
		 * Activity 에 Attach 되어 있지않으면
		 * getResources()를 실행할 수 없으며, 플레이어 재생이 불가능하므로
		 * 실행하지 않고 null 반환
		 */
		if (!isAdded()) {
			return null;
		}

        String ret = "mimacstudy" + "://player?"
                + "siteid=%s"
                + "&userid=%s"
                + "&playid=%s"
                + "&reqversion="
                + "&lmsurl=%s"
                + "&lmsreqtime=%s"
                + "&authurl=%s"
                + "&authreqtime=%s"
                + "&mrl=%s"
                + "&playtitle=%s"
                + "&durationtime=%s"
                + "&playcurtime=%s"
                + "&studytime=%s"
                + "&progresstime=%s"
                + "&bookmarklist=%s"
                + "&extinfo=%s"
                + "&islocalplay=1"
                + "&fileid=%s"
                + "&lmsid=%s"
                + "&drmauthurl=%s"
                + "&drmauthtime=%s"
                + "&drmauthid=%s"
                + "&deviceinfo=%s"
                + "&subtitlespath=%s"
                + "&subtitlescharset=%s"
                + "&courseid=%s"
                + "&coursename=%s"
                + "&courseseq=%s"
                + "&teachername=%s"
                + "&lectureid=%s"
                + "&lecturename=%s"
                + "&lectureseq=%s"
                + "&etcinfo=%s"
                ;

        // ext-info 는 URL Encoding(UTF-8)
        String encodedExtInfo = "";
        if (data.extInfo != null) {
            try {
                encodedExtInfo = URLEncoder.encode(data.extInfo, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        // 이어보기 -5초 처리(이전 재생 시점 인지 처리)
        int intPlayCurTime;
        try {
            intPlayCurTime = Integer.parseInt(data.playCurTime);
        } catch (NumberFormatException e) {
            intPlayCurTime = 0;
        }

        if (intPlayCurTime > 5) {
            intPlayCurTime -= 5;
        }

        return String.format(ret,
                data.siteId,
                data.userId,
                data.playId,
                data.lmsUrl,
                data.lmsReqTime,
                data.authUrl,
                data.authReqTime,
                playUrl,
                data.fileTitle,
                data.totalLength,
                Integer.toString(intPlayCurTime),
                "", //data.studyTime,
                "", //data.progressTime,
                getBookmarkString(data.arrayListBookmark),
                encodedExtInfo,
                data.fileId,
                data.lmsId,
                data.drmAuthUrl,
                data.drmAuthTime,
                data.drmAuthId,
                LibUtil.getAndroidId(getContext()),
                data.subtitlesPath,
                data.subtitlesCharset,
                data.courseId,
                data.courseName,
                data.courseIndex,
                data.teacherName,
                data.lectureId,
                data.lectureName,
                data.lectureIndex,
                encodedExtInfo
        );
    }

    /**
     * ArrayList 타입의 북마크 데이터를 ","를 구분자로 갖는 String 으로 반환
     * @param arrayList	북마크 ArrayList
     * @return	북마크 String
     */
    private String getBookmarkString(ArrayList<String> arrayList) {

        StringBuilder stringBuilder = new StringBuilder();

        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            stringBuilder.append(it.next());
            if (it.hasNext()) {
                stringBuilder.append(",");
            }
        }

        return stringBuilder.toString();
    }

    /**
     * 다운로드 완료된 파일 재생
     * @param data	다운로드 완료된 ZoneDownloadData
     */
    private void playContent(ZoneDownloadData data) {

        // 현재 콘텐츠 파일 path 가 내장 또는 외장인지 확인
        String path =
                (StringUtil.contains(data.filePath,
                        ContentFileManager.getInstance(getContext()).mExtSDCardPath)
                        ? ContentFileManager.getInstance(getContext()).mExtSDCardPath
                        : ContentFileManager.getInstance(getContext()).mIntSDCardPath);

        // 외장 유효 여부 확인 후 유효하지 않으면 오류 Alert 표출
        if (! ContentFileManager.getInstance(getContext()).isAvailSpace(path)) {

            dialogNoExtSDCard();
            return;
        }

        // 저장된 DB (이어보기 / 북마크 등 기타 정보)에서 실행 데이터 획득
        ZoneDownloadData cbData = ContentsDatabase2.getInstance(getContext()).getFileInfoDetail(data.fileId);

        if ((cbData != null)) {

            // 인증 DRM 파일
            if (cbData.isCert > 0) {

                // 기기인증 후 재생
                new DRMAuthDeviceAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, cbData);

                // NonDRM 파일
            } else {

                // 다운로드 데이터의 재생 경로 변환
                String filepath = "file://" + cbData.filePath;

                // 다운로드 데이터를 재생 데이터 형식으로 변환
                String url = convertZonePlayerData(cbData, filepath);

                // ZonePlayerData 가 없으면 실행하지 않고 종료
                if (! StringUtil.isNotBlank(url)) {
                    return;
                }

                Intent intent = null;
                // Intent Scheme 실행
                try {
                    intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                if (intent != null) {
                    try {

                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.putExtra(Browser.EXTRA_APPLICATION_ID, getContext().getPackageName());
                        intent.setPackage(getContext().getPackageName());

                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {

                        // 앱이 설치되어 있지 않으면 Play 스토어 설치 페이지로 전환
                        Uri uri = Uri.parse(getResources().getString(R.string.url_playstore) + intent.getPackage());
                        Intent storeIntent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(storeIntent);
                    }
                }
            }

        } else {
            LibUtil.toaster(getContext(), "저장된 콘텐츠가 없습니다. 다시 다운로드 후 실행하세요.");
        }
    }

    /**
     * 미완료된 다운로드 아이템을 모두 취소 상태로 설정
     */
    private void setDeleteAll() {

        // 다운로드 서비스 요청 상태에서는 이벤트 실행하지 않음
        if (mDownloadServiceRequest) {
            Logger.d("download-service request status");
            return;
        }

        // 순차 파일 삭제
        for (ZoneDownloadReqData data : mListDownloadReq) {

            // 다운로드 완료된 파일이 아닌 경우에만 파일 삭제
            if (data.downloadResult != RESULT_SUCCESS) {
                ContentFileManager.getInstance(getContext()).deleteFile(data.filePath);
            }
        }

        // 다운로드 요청 테이블 삭제
        mDb.deleteAllDownloadReq();

        // ArrayList 전체 삭제
        mListDownloadReq.clear();
        mListListener.clear();

        mLayoutProgress.removeAllViews();

        // 삭제 처리가 시작되면 Action Mode 원복
        toggleDeleteMode();
    }

    /**
     * Notification 표출
     * Handler 에 의해 실행되므로 별도의 Thread 필요 없음
     * @param title	타이틀
     */
    private void startNotification(String title) {

        CancelNotificationReceiver.isCancelNotification = false;

        NotificationManager nMgr = (NotificationManager)
                IMGApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // 실행 Intent 생성
        Intent intent = makeDownloadProgressIntent();
        PendingIntent contentIntent = PendingIntent.getActivity(IMGApplication.getContext(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // 종료 Intent 생성
        Intent closeIntent = new Intent(ACTION_RELEASE_NOTIFICATION);
        PendingIntent closePendingIntent = PendingIntent.getBroadcast(IMGApplication.getContext(), 0,
                closeIntent, 0);

        // RemoteViews 생성 및 초기화
        RemoteViews notificationProgressView = new RemoteViews(IMGApplication.getContext().getPackageName()
                , R.layout.notification_progress);

        notificationProgressView.setTextViewText(R.id.tv_title, title);
        notificationProgressView.setTextViewText(R.id.tv_size, "");
        notificationProgressView.setTextViewText(R.id.tv_percent, "0%");
        notificationProgressView.setProgressBar(R.id.pb_download_item, 100, 0, true);
        notificationProgressView.setOnClickPendingIntent(R.id.btn_close, closePendingIntent);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(IMGApplication.getContext());

        notificationBuilder
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_inbox_w)
                .setContentIntent(contentIntent)
                .setTicker("다운로드중...")
//        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        ;

        notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.contentView = notificationProgressView;

        nMgr.notify(NOTIFICATION_ID, notification);
    }

    /**
     * 다운로드 아이템의 다운로드 프로그레스 업데이트
     * @param size	현재까지 다운로드된 파일 사이즈
     * @param progress	업데이트할 프로그레스
     */
    private void updateProgressNotification(String size, int progress) {

        // Notification 의 취소키를 선택하면 Progress 표출하지 않음
        if (CancelNotificationReceiver.isCancelNotification) {
            return;
        }

        NotificationManager nMgr = (NotificationManager)
                IMGApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if ((nMgr != null) && (notification != null)) {

            notification.contentView.setTextViewText(R.id.tv_size, size);
            notification.contentView.setTextViewText(R.id.tv_percent, progress + "%");
            notification.contentView.setProgressBar(R.id.pb_download_item, 100, progress, false);

            nMgr.notify(NOTIFICATION_ID, notification);
        }
    }

    /**
     * Notification 초기화 및 해제
     */
    private void releaseNotification() {

        // Notification 취소 여부 초기화
        CancelNotificationReceiver.isCancelNotification = false;

        NotificationManager nm = (NotificationManager)
                IMGApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        nm.cancel(NOTIFICATION_ID);
    }

    /**
     * data = blank 를 넣은 Download Progress Intent 생성
     * @return	 Download Progress Intent
     */
    private Intent makeDownloadProgressIntent() {

        Intent intent = null;

        String uri = null;
        try {
            uri = String.format("mimacstudy"
                            + "://download_working?"
                            + "data=%s",
                    Base64.encodeToString(("blank").getBytes("UTF-8"), Base64.DEFAULT)
            );
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        if (uri != null) {
            // Intent 설정
            try {
                intent = Intent.parseUri(uri, Intent.URI_INTENT_SCHEME);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, IMGApplication.getContext().getPackageName());
                intent.setPackage(IMGApplication.getContext().getPackageName());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        return intent;
    }

    /**
     * Play 스토어로 업데이트 링크
     */
    private void updateStore() {

        getActivity().finish();

        Uri uri = Uri.parse(getResources().getString(R.string.url_playstore) + getActivity().getPackageName());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    /**
     * 다운로드 요청 데이터 존재 여부 반환
     * @return  true: 다운로드 요청 데이터 반환. false: 다운로드 요청 데이터 없음
     */
    private boolean existDownloadReq() {

        ArrayList<ZoneDownloadReqData> list = mDb.getDownloadReqInfo();

        return (list != null) && (list.size() > 0);
    }

    /**
     * 최종 다운로드 상태 저장
     */
    private void saveLastDownloadReqStatus() {

        // 1. 다운로드 완료된 아이템 삭제
        for (int i = 0; i < mListDownloadReq.size(); i++) {
            if (mListDownloadReq.get(i).downloadResult == RESULT_SUCCESS) {

                removeDownloadItem(i);
            }
        }

        // 2. 기존 다운로드 DB 테이블 삭제
        mDb.deleteAllDownloadReq();

        // 현재 리스트에 있는 다운로드 데이터를 DB에 재저장
        // index 및 order 재설정
        for (int i = 0; i < mListDownloadReq.size(); i++) {

            ZoneDownloadReqData data = mListDownloadReq.get(i);

            // 다운로드 시간 및 다운로드 ID 유지, 다운로드 순서만 다시 저장.
            mDb.addDownloadReqInfo(data, data.downloadId, i, data.downloadTime);
        }
    }

    /**
     * 다운로드 ID 로 현재 리스트에서 인덱스 검색
     * @param downloadId 검색할 다운로드 ID
     * @return 인덱스
     */
    private int getIndexByDownloadId(int downloadId) {

        int ret = -1;

        for (int i = 0; i < mListDownloadReq.size(); i++) {
            if (downloadId == mListDownloadReq.get(i).downloadId) {
                ret = i;
                break;
            }
        }

        return ret;
    }

    /**
     * 설정된 저장 매체의 공간이 유효한 공간인지 여부 반환
     * @return  true 유효 공간. false 공간 부족
     */
    private boolean isUsableSpace() {

        long usable = ContentFileManager.getInstance(IMGApplication.getContext()).getCurrentUsableSpace();

        Logger.d("usable-space: " + usable);

        // 1024000000f
        return (usable >= AVAIL_USABLE_SPACE);
    }

    /**
     * 전체 삭제 확인 알림
     */
    private void dialogDeleteAll() {

        if (getActivity() != null) {

            // 활성 다운로드 있음
            //if (mActiveDownload != null) {
            try {
                if(mService.getActiveDownload() != null) {
                    //handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);
                    //dialogExistActiveDownload();
                    dialogConfirmDeleteAllDownload();
                    return;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            if (existDownloadReq()) {
                mDialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_DELETE_ALL, this);
                mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
            } else {
                dialogNoDownload();
            }
        }
    }

    /**
     * 다운로드 완료 Alert 표출
     */
    private void dialogDownloadComplete() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_COMPLETE, this);
            //dialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);

        	/*
        	 * 다운로드 중 재생시(다운로드 화면이 Background 상태) show()를 직접 호출하면
        	 * IllegalStateException Can not perform this action after onSaveInstanceState with DialogFragment
        	 * 이 발생할 수 있으므로 아래와 같이 적용
        	 */
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.add(mDialog, DownloadDialog.DIALOG_TAG);
            ft.commitAllowingStateLoss();
        }
    }

    /**
     * 네트워크 데이터 과금 Alert 표출
     */
    private void dialogNetworkAlert() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_NETWORK_WARNING, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 업데이트 확인 표출
     */
    private void dialogUpdateAlert() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_UPDATE_YESNO, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 외장 sd 카드 없음 alert 표출
     */
    private void dialogNoExtSDCard() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_NO_EXT_SDCARD, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 활성 다운로드 삭제 alert 표출
     * @param position 삭제할 item index
     */
    private void dialogConfirmDeleteDownload(int position) {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_CONFIRM_DELETE_DOWNLOAD, position, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 활성 다운로드를 포함한 전체 삭제 alert 표출
     */
    private void dialogConfirmDeleteAllDownload() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_CONFIRM_DELETE_ALL, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 중복 다운로드 있음 alert 표출
     * @param total     전체 개수
     * @param duplicate 중복 개수
     */
    @SuppressLint("DefaultLocale")
    private void dialogDownloadCount(int total, int duplicate) {

        // 총 41개 중 이미 다운로드 된 N개를 제외한 40개의 콘텐츠가 다운로드 목록에 추가 되었습니다.
        // 이미 다운로드 된 콘텐츠 입니다. \n보관함을 확인하세요.
        // 총 41개 중 41개 콘텐츠가 다운로드 목록에 추가 되었습니다.
        if (getActivity() != null) {
            String message;

            if (duplicate == 0) {

                // 총 %d 개 중 %d 개 다운로드 목록 추가
                message = String.format("총 %d 개 중 %d 개 다운로드 목록 추가",
                        total,
                        total);
            } else {

                if (total == duplicate) {

                    message = "이미 다운로드 되었거나 다운로드 중인 콘텐츠 입니다. 다운로드 목록이나 보관함을 확인하세요.";

                } else {

                    // 총 %d 개 중 %d 개 다운로드 목록 추가. %d 개는 다운로드 목록이나 보관함을 확인하세요.
                    message = String.format("총 %d 개 중 %d 개 다운로드 목록 추가. %d 개는 다운로드 목록이나 보관함을 확인하세요.",
                            total, (total - duplicate), duplicate);
                }
            }

            //mDialog = DownloadDialog.getInstance(DIALOG_DOWNLOAD_COUNT, -1, message, this);
            //mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);

            LibUtil.toaster(IMGApplication.getContext(), message, Toast.LENGTH_LONG);
        }
    }

    /**
     * 유효 공간 부족 alert
     */
    private void dialogUnusableSpace() {

        handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);

        if (getActivity() != null) {

            // 저장소 제거로 인한 표출 차단
            if (mMediaMountError) {
                return;
            }

            mDialog = DownloadDialog.getInstance(DIALOG_UNUSABLE_SPACE, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 다른 활성 다운로드 존재 알림
     */
    private void dialogExistActiveDownload() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_EXIST_ACTIVE_DOWNLOAD, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 다운로드 항목 없음 표출
     */
    private void dialogNoDownload() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_NO_DOWNLOAD, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 네트워크 없음 알림
     */
    private void dialogNoNetwork(int index) {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_NO_NETWORK, index, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 미디어 마운트 에러(저장소 제거) 알림
     */
    private void dialogMediaMountError() {

        if (getActivity() != null) {

            mDialog = DownloadDialog.getInstance(DIALOG_MEDIA_MOUNT_ERROR, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * 저장소 위치 다름
     */
    private void dialogNotMatchingLocation(String msg) {

        if (getActivity() != null) {

            String message = String.format("%s저장소 다운로드 콘텐츠입니다. 설정에서 강의 저장 공간 변경 후 시도하세요.", msg);

            mDialog = DownloadDialog.getInstance(DIALOG_NOT_MATCHING_LOCATION, 0, message, this);
            mDialog.show(getActivity().getSupportFragmentManager(), DownloadDialog.DIALOG_TAG);
        }
    }

    /**
     * ProgressDialog 표출
     */
    private void dialogWaiting() {

        if (getActivity() != null) {

            mDownloadServiceRequest = true;

            mProgressingDialog = ProgressingDialog.getInstance(0);
            mProgressingDialog.show(getActivity().getSupportFragmentManager(), ProgressingDialog.DIALOG_TAG);
        }
    }

    /**
     * ProgressDialog 해제
     */
    private void dismissWaiting() {

        mDownloadServiceRequest = false;

        if (getActivity() == null) {
            return;
        }

        if (mProgressingDialog != null) {
            mProgressingDialog.dismiss();
            mProgressingDialog = null;
        }
    }

    /**
     * Intent 데이터 및 저장 데이터 처리 AsyncTask
     * @author kimsanghwan
     * @since 2015.11.02.
     */
    private class DataProcessingTask extends AsyncTask<Void, Void, ArrayList<ZoneDownloadReqData>> {

        // 신규 추가 다운로드 진행 인덱스
        private int startIndex = -1;

        private int duplicatedCount = 0;    // 중복 요청 다운로드 개수
        private int totalCount = 0;         // 전체 요청 다운로드 개수

        // 신규 추가 다운로드 요청 리스트
        private ArrayList<ZoneDownloadReqData> listNewItem = new ArrayList<>();

        @Override
        protected ArrayList<ZoneDownloadReqData> doInBackground(Void... params) {

            // 1. 전달된 Intent Parsing
            IntentDataParser.IntentDataParserResult2 result = initIntentDataList3();

            // 3. 전달 Intent 와 기존 저장된 다운로드 데이터 비교
            if (result.code == RES_OK) {

                totalCount = result.listDownloadData.size();

                for (ZoneDownloadReqData data : result.listDownloadData) {

                    // 중복 다운로드 요청 처리
                    if (mDb.existDownloadReqInfo(data) || mDb.existFileByDownloadReq(data)) {

                        // 중복 다운로드 개수 누적
                        duplicatedCount++;

                        // 신규 다운로드 요청 처리
                    } else {

                        // 최종 다운로드 인덱스 획득
                        int lastIndex = mDb.getLastIndexDownloadReq();

                        // 신규 다운로드 아이템 중 1st 아이템의 인덱스 설정
                        if (startIndex < 0) {

                            // onPostExecute() 에서 mListDownloadReq.add() 예정이므로 현재 getDownloadReqInfoCount() 설정
                            startIndex = mDb.getDownloadReqInfoCount();
                        }

                        // 다운로드 요청 데이터를 DB에 추가
                        mDb.addDownloadReqInfo(data, (++lastIndex), lastIndex, System.currentTimeMillis());

                        // 신규 다운로드 요청 데이터 추가
                        listNewItem.add(data);
                    }

                }

            }

            return mDb.getDownloadReqInfo();
        }

        @Override
        protected void onPostExecute(ArrayList<ZoneDownloadReqData> list) {

            super.onPostExecute(list);

            // 다운로드 요청 결과가 없으면 처리 완료
            if (list == null) {
                return;
            }

            // 신규 다운로드 데이터를 RecycledView 에 추가
            for (ZoneDownloadReqData data : listNewItem) {

                // 다운로드 아이템 리스너 생성
                DownloadItemListener listener = new DownloadItemListener();

                // RecycledView 에 추가
                addDownloadItem(data, listener, true);
            }

            // 기존 활성 다운로드 획득
            try {
                mActiveDownload = mService.getActiveDownload();
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            // 기존 활성 다운로드 있음
            if (mActiveDownload != null) {

                // ActiveDownload 를 해당 위치에 교체 삽입
                for (int i = 0; i < mListDownloadReq.size(); i++) {

                    int downloadId = mListDownloadReq.get(i).downloadId;
                    if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadId == downloadId) {

                        ((DownloadItemListener) (mActiveDownload.DOWNLOAD_LISTENER))
                                .setView(mListListener.get(i).view);
                        mUpdateProgress = false;

                        break;
                    }
                }

            // 기존 활성 다운로드 없음
            } else {

                // 인텐트 데이터 파싱 정상 완료 후 신규 다운로드 아이템 존재
                if (startIndex > -1) {

                    // Message 전달
                    handler.removeMessages(MSG_START_DOWNLOAD_SERVICE);

                    Message message = handler.obtainMessage(MSG_START_DOWNLOAD_SERVICE, startIndex, -1);
                    handler.sendMessageDelayed(message, 300);	// Message 전달
                }
            }

            // 신규 다운로드 요청이 있음
            if (totalCount > 0) {

                dialogDownloadCount(totalCount, duplicatedCount);
            }

            /*
			 * mBundle 및 mIntent 초기화
			 * 전달된 Intent 의 파싱 처리가 완료되었으므로, 기존 Bundle 및 Intent 초기화
			 * @date 2015. 03. 19.
			 */
            mIntent = null;
            mBundle = null;
        }
    }

    /**
     * DRM 기기인증 AsyncTask
     */
    private class DRMAuthDeviceAsyncTask extends AsyncTask<ZoneDownloadData, Void, JSONObject> {

        private ZoneDownloadData data;

        @Override
        protected JSONObject doInBackground(ZoneDownloadData... params) {

            if ((params != null) && (params[0] != null)) {

                data = params[0];

                NetworkManager netMgr = new NetworkManager(getContext());

                String url = makeDRMAuthUrl(params[0]);

                if (url != null) {

                    try {
                        String res = netMgr.httpGet(url);

                        JSONObject retJson;
                        try {
                            retJson = new JSONObject(res);
                        } catch (JSONException e) {
                            Logger.d("drm auth response: " + res);
                            return null;
                        }

                        return retJson;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            parseDRMAuthResponse(result);
        }

        /**
         * DRM 기기인증을 위한 전체 URL 생성 반환
         * @param data	ZoneDownloadData
         * @return	DRM 기기인증을 위한 전체 URL
         */
        private String makeDRMAuthUrl(ZoneDownloadData data) {

            String ret = null;
            String token = "?";

            if ((data != null)
                    && (StringUtil.isNotBlank(data.userId))
                    && (StringUtil.isNotBlank(data.drmAuthUrl))
                    && (StringUtil.isNotBlank(data.drmAuthId))) {

                if (data.drmAuthUrl.contains(token)) {
                    token = "&";	// 기존 URL 에 연결된 데이터가 있으면 연결자 변경
                }

                // 서버에 재생 정보 요청 URL 생성
                ret = data.drmAuthUrl
                        + token + USER_ID + "=" + data.userId
                        + "&" + DRM_AUTH_ID + "=" + data.drmAuthId
                        + "&" + DEVICE_INFO + "=" + LibUtil.getAndroidId(getContext())
                        + "&" + STATUS + "=0"	// 재생전(0)
                ;
            }

            return ret;
        }

        /**
         * DRM 기기인증 응답 처리
         * @param json	인증 응답 JSONObject
         */
        private void parseDRMAuthResponse(JSONObject json) {

            if (json != null) {

                String code = null, message = null;

                try {
                    if (json.has(RESPONSE_CODE)) {
                        code = json.getString(RESPONSE_CODE);
                    }
                    if (json.has(RESPONSE_MESSAGE)) {
                        message = json.getString(RESPONSE_MESSAGE);
                    }

                    if (StringUtil.equals(code, "0")) {
                        // 다운로드 진행 화면이므로 프로세스에 지장이 없도록 토스트 메시지 표출
                        LibUtil.toaster(getContext(), message);
                        return;
                    }

                } catch (JSONException e) {
                    Logger.d("drm auth response json fail: " + json.toString());
                }
            }

            //String filepath = setZoneHttpDContentUrl(data.filePath);
            String filepath =
                    "http://localhost:" + IMGApplication.mZoneHttpDPort + data.filePath;

            // 다운로드 데이터를 재생 데이터 형식으로 변환
            String url = convertZonePlayerData(data, filepath);

            // ZonePlayerData 가 없으면 실행하지 않고 종료
            if (! StringUtil.isNotBlank(url)) {
                return;
            }

            Intent intent = null;
            // Intent Scheme 실행
            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            if (intent != null) {
                try {

                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.putExtra(Browser.EXTRA_APPLICATION_ID, getContext().getPackageName());
                    intent.setPackage(getContext().getPackageName());

                    startActivity(intent);
                } catch (ActivityNotFoundException e) {

                    // 앱이 설치되어 있지 않으면 Play 스토어 설치 페이지로 전환
                    Uri uri = Uri.parse(getResources().getString(R.string.url_playstore) + intent.getPackage());
                    Intent storeIntent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(storeIntent);
                }
            }
        }
    }

    /**
     * 다운로드 아이템 이벤트 리스너
     */
    private class DownloadItemListener implements IDownloadItemListener {

        private int position;
        private View view;

        public void setView(int index, View view) {

            this.position = index;
            this.view = view;
        }

        public void setView(View view) {

            this.view = view;
        }

        @Override
        public void onUpdateProgress(int downloadId, long total, long progress)
                throws RemoteException {

            if (view == null) {
                Logger.d("view is null");
                return;
            }

            int index = getIndexByDownloadId(downloadId);

            if (index > -1) {

                // 1. 다운로드 데이터 설정
                ZoneDownloadReqData reqData = mListDownloadReq.get(index);
                reqData.fileSize = Long.toString(total);
                reqData.downloadSize = progress;

                /*if (index != position) {
                    Logger.d("index != position 발생!");
                    return;
                }*/


                // Message 생성
                /*Message message = handler.obtainMessage(MSG_UPDATE_PROGRESS, index, -1, view);	// Progress Index 설정
                handler.sendMessageDelayed(message, TIME_UPDATE_PROGRESS);	// Message 전달*/
                if (! mUpdateProgress) {
                    mUpdateProgress = true;

                    /*if (index != position) {
                        Logger.d("index != position 발생!");
                        return;
                    }*/

                    // Message 생성
                    Message message = handler.obtainMessage(MSG_UPDATE_PROGRESS, index, -1, view);	// Progress Index 설정
                    handler.sendMessageDelayed(message, TIME_UPDATE_PROGRESS);	// Message 전달
                }
            }
        }

        @Override
        public void onNotifyStatus(int downloadId, int status, int result, String msg) throws RemoteException {

            if (view == null) {
                return;
            }

            // 다운로드 id로 현재 list index 획득
            int index = getIndexByDownloadId(downloadId);

            Logger.d("[" + index + "] status: " + status + ", result: " + result + ", msg: " + msg);

            if (index > -1) {

                // 1. 다운로드 데이터 설정
                ZoneDownloadReqData reqData = mListDownloadReq.get(index);
                reqData.downloadStatus = status;
                reqData.downloadResult = result;
                reqData.downloadMsg = msg;

                // 2. Message 생성 및 전달
                Message message = handler.obtainMessage(MSG_NOTIFY_STATUS, index, status, view);

                Bundle data = new Bundle();
                data.putInt(KEY_MSG_STATUS, status);
                data.putString(KEY_MSG_MESSAGE, msg);

                message.setData(data);

                handler.sendMessage(message);
            }
        }

        @Override
        public void onGetFileInformation(int downloadId, int status, int result,
                                         long totalSize, long createDate, long startDate, long endDate)
                throws RemoteException {

            if (view == null) {
                return;
            }

            // 다운로드 id로 현재 list index 획득
            int index = getIndexByDownloadId(downloadId);

            Logger.d("[" + index + "] status: " + status + ", result: " + result + ", totalSize: " + totalSize + ", createData: " + createDate);

            if (index > -1) {

                // 1. 다운로드 데이터 설정
                ZoneDownloadReqData reqData = mListDownloadReq.get(index);
                reqData.downloadStatus = status;
                reqData.downloadResult = result;

                if (totalSize > 0) {
                    reqData.fileSize = Long.toString(totalSize);
                }
                if (createDate > 0) {
                    reqData.createTime = createDate;
                }
                if (startDate > 0) {
                    reqData.startTime = startDate;
                }
                if (endDate > 0) {
                    reqData.endTime = endDate;
                }

                // 2. 메시지 생성 및 전달
                Message message = handler.obtainMessage(MSG_FILE_INFORMATION, index, status, view);
                handler.sendMessage(message);
            }
        }

        @Override
        public IBinder asBinder() {
            return null;
        }
    }

    /**
     * 다운로드 정지/재개 리스너
     * @author kimsanghwan
     * @since 2015-11-17
     */
    private class DownloadStopResumeListener implements OnClickListener {

        private int downloadId;     // 다운로드 id
        private ImageView viewStopResume;// 다운로드 정지 재개 버튼

        public DownloadStopResumeListener(int id,View view) {

            downloadId = id;
            viewStopResume = (ImageView) view;
        }

        @Override
        public void onClick(View v) {

            // 다운로드 ID 로 현재 다운로드 요청 목록 인덱스 조회
            int index = getIndexByDownloadId(downloadId);

            // 활성 다운로드 있음
            if (mActiveDownload != null) {

                // 현재 활성 다운로드
                if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadId == downloadId) {

                    // 다운로드 서비스 요청 상태에서는 이벤트 실행하지 않음
                    if (mDownloadServiceRequest) {
                        Logger.d("download-service request status");
                        return;
                    }

                    handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);

                    viewStopResume.setImageResource(R.drawable.ic_get_g);

                // 활성 다운로드 아님
                } else {

                   dialogExistActiveDownload();
                }

            // 활성 다운로드 없음
            } else {

                // 네트워크 미연결
                if (! LibUtil.isNetworkAvailable(getContext())) {

                    // 네트워크 없음 알림
                    dialogNoNetwork(index);
                    return;
                }

                // 설정: 외부 저장소
                if (ConfigurationManager.getUseExtSDCard(getContext())) {

                    // 파일 위치: 내부
                    if (mListDownloadReq.get(index).downloadLocation == LOCATION_INNER) {
                        dialogNotMatchingLocation("내부");
                        return;
                    }

                // 설정: 내부 저장소
                } else {

                    // 파일 위치: 외부
                    if (mListDownloadReq.get(index).downloadLocation == LOCATION_EXT1) {
                        dialogNotMatchingLocation("외부");
                        return;
                    }

                }

                Message message = handler.obtainMessage(MSG_START_RESUME_DOWNLOAD, index, -1);
                handler.sendMessageDelayed(message, 1000);	// Message 전달
            }
        }
    }

    /**
     * 다운로드 요청 아이템 삭제 리스너
     * @author kimsanghwan
     * @since 2015-11-18
     */
    private class RemoveItemListener implements OnClickListener {

        private int downloadId;

        /**
         * 생성자
         * @param id    download-id
         */
        RemoveItemListener(int id) {

            downloadId = id;
        }

        @Override
        public void onClick(View v) {

            // 다운로드 ID 로 현재 다운로드 요청 목록 인덱스 조회
            int index = getIndexByDownloadId(downloadId);

            // 활성 다운로드 있음
            if (mActiveDownload != null) {

                // 활성 다운로드 삭제 시도
                if (mActiveDownload.DOWNLOAD_REQ_DATA.downloadId == downloadId) {
                    dialogConfirmDeleteDownload(index);
                } else {

                    /*
                     * 파일경로가 존재하면 삭제
                     */
                    String filePath = mListDownloadReq.get(index).filePath;
                    if (StringUtil.isNotBlank(filePath)) {
                        ContentFileManager.getInstance(getContext()).deleteFile(filePath);
                    }

                    removeDownloadItem(index);
                }

            } else {
                /*
                 * 파일경로가 존재하면 삭제
                 */
                String filePath = mListDownloadReq.get(index).filePath;
                if (StringUtil.isNotBlank(filePath)) {
                    ContentFileManager.getInstance(getContext()).deleteFile(filePath);
                }

                removeDownloadItem(index);
            }

        }
    }

    /**
     * 다운로드 아이템의 재생 터치 리스너
     */
    private class PlayContentClickListener implements OnClickListener {

        private ZoneDownloadData data;

        /**
         * 생성자
         *
         * @param d ZoneDownloadData
         */
        public PlayContentClickListener(ZoneDownloadData d) {

            data = d;
        }

        @Override
        public void onClick(View v) {

            playContent(data);
        }
    }

    /**
     * 네트워크 상태 변화를 수신하는 BroadcastReceiver
     * @author kimsanghwan
     * @since 2014. 3. 27.
     */
    private class NetworkStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if ((intent != null) && (getContext() != null)) {

                String action = intent.getAction();

                if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

                    ConnectivityManager conMgr = (ConnectivityManager) IMGApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

                    if (netInfo == null) {

                        //viewNetworkError();
                        mNetworkOn = false;
                        LibUtil.toaster(IMGApplication.getContext(), "네트워크 오류");
                        return;
                    }

                    if (netInfo.isAvailable()) {

                        mNetworkOn = true;
/*

                        if (mDownloadStop) {
                            // todo 사용자가 임의 정지했으므로 다운로드 재시작하지 않음
                            return;
                        }
*/

                    } else {

                        //viewNetworkError();
                        mNetworkOn = false;

                        LibUtil.toaster(IMGApplication.getContext(), "네트워크 오류");
                    }
                }
            }

        }

    }

    /**
     * 미디어 마운트 리시버
     * @author kimsanghwan
     * @since 2015-11-20
     */
    private class MediaMountReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String strAction = intent.getAction();

            if (IMGApplication.getContext() != null) {

                // SD Card Mounted
                if (strAction.compareTo(Intent.ACTION_MEDIA_SCANNER_FINISHED) == 0) {
                    LibUtil.toaster(IMGApplication.getContext(), "media-scan finished");
                    // Logger.d("media scan finished");
                } else if (strAction.compareTo(Intent.ACTION_MEDIA_EJECT) == 0) {
                    // Util.toaster(IMGApplication.getAppContext(), "media unmounted");

                    mMediaMountError = true;
                    dialogMediaMountError();
                    ConfigurationManager.setUseExtSDCard(IMGApplication.getContext(), false);
                    ContentFileManager.getInstance(IMGApplication.getContext()).setUseExtSDCard(false);

                    handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);

                } else if (strAction.compareTo(Intent.ACTION_MEDIA_MOUNTED) == 0) {
                    LibUtil.toaster(IMGApplication.getContext(), "media mounted");
                    mMediaMountError = false;
                } else if (strAction.compareTo(Intent.ACTION_MEDIA_UNMOUNTED) == 0) {
                    mMediaMountError = true;
                    //dialogMediaMountError();
                    ConfigurationManager.setUseExtSDCard(IMGApplication.getContext(), false);
                    ContentFileManager.getInstance(IMGApplication.getContext()).setUseExtSDCard(false);
                    LibUtil.toaster(IMGApplication.getContext(), "media unmounted");

                    handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);

                }  else if (strAction.compareTo(Intent.ACTION_MEDIA_REMOVED) == 0) {
                    mMediaMountError = true;
                    //dialogMediaMountError();
                    ConfigurationManager.setUseExtSDCard(IMGApplication.getContext(), false);
                    ContentFileManager.getInstance(IMGApplication.getContext()).setUseExtSDCard(false);
                    LibUtil.toaster(IMGApplication.getContext(), "media removed");

                    handler.sendEmptyMessage(MSG_STOP_DOWNLOAD_SERVICE);
                }
            }

        }
    }

    /**
     * 다운로드 Progress 관리 Handler
     * @author kimsanghwan
     * @since 2014. 8. 29.
     */
    private static class DownloadProgressHandler extends WeakHandler<DownloadProgressFragment> {

        public DownloadProgressHandler(DownloadProgressFragment owner) {
            super(owner);
        }

        @Override
        public void handleMessage(Message msg) {

            DownloadProgressFragment owner = getOwner();

            switch (msg.what) {

                // 다운로드 진행 Progress 업데이트 메시지
                case MSG_UPDATE_PROGRESS:

                    owner.progressItemView((View) msg.obj, msg.arg1);
                    mUpdateProgress = false;
                    break;

                // 다운로드 STEP 메시지
                case MSG_NOTIFY_STATUS:

                    owner.statusItemView((View) msg.obj, msg.arg1);
                    break;

                // 인증서 및 파일 정보 전달 메시지
                case MSG_FILE_INFORMATION:

                    owner.infoItemView((View) msg.obj, msg.arg1);
                    break;

                // 다운로드 아이템 뷰 표출
                case MSG_START_DOWNLOAD_PROGRESS:

                    // 전달받은 다운로드 Intent 시작
                    owner.startDownloadProgress();
                    break;

                // Notification 해제
                case MSG_END_NOTIFICATION:

                    if (owner != null) {
                        owner.releaseNotification();
                    }
                    break;

                // 다운로드 서비스 시작
                case MSG_START_DOWNLOAD_SERVICE:

                    // 다운로드 유효 공간 확인
                    if (!owner.isUsableSpace()) {

                        owner.dialogUnusableSpace();
                        return;
                    }

                    mMediaMountError = false;

                    // 활성 다운로드 생성
                    mActiveDownload = new ActiveDownload(
                            mListDownloadReq.get(msg.arg1),
                            mListListener.get(msg.arg1));

                    mUpdateProgress = false;

                    // 서비스 시작
                    try {
                        mService.setActiveDownload(mActiveDownload);
                        mService.startDownload();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    // notification 시작
                    owner.startNotification(mActiveDownload.DOWNLOAD_REQ_DATA.fileTitle);

                    break;

                // 다운로드 서비스 정지
                case MSG_STOP_DOWNLOAD_SERVICE:

                    //owner.dialogWaiting();

                    try {
                        mService.stopDownload();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    //
                    /*
                     * progress-update 가 delay 부여되어 있으므로
                     * update 완료 후 처리하도록 1000ms delay 부여
                     */
                    handler.sendEmptyMessageDelayed(MSG_END_NOTIFICATION, 1000);
                    break;

                // 다운로드 재개 시작
                case MSG_START_RESUME_DOWNLOAD:

                    // 다운로드 유효 공간 확인
                    if (!owner.isUsableSpace()) {

                        owner.dialogUnusableSpace();
                        return;
                    }

                    // 활성 다운로드 생성
                    mActiveDownload = new ActiveDownload(
                            mListDownloadReq.get(msg.arg1),
                            mListListener.get(msg.arg1));

                    owner.mUpdateProgress = false;

                    // 서비스 시작
                    try {
                        mService.setActiveDownload(mActiveDownload);
                        mService.resumeDownload();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    // notification 시작
                    owner.startNotification(mActiveDownload.DOWNLOAD_REQ_DATA.fileTitle);

                    break;

                case MSG_ADD_ROW_VIEW:
                    owner.mLayoutProgress.addView((View)(msg.obj));
                    break;

            }
        }
    }
}
