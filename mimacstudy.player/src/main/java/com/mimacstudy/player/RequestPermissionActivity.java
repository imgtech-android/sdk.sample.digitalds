package com.mimacstudy.player;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class RequestPermissionActivity extends AppCompatActivity implements View.OnClickListener {

    private View mBtnAllow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_request_permission);

        mBtnAllow = findViewById(R.id.btn_allow);
        mBtnAllow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == mBtnAllow) {

            SettingsManager.setAllowPermission(this, true);

            Intent intent = new Intent(this, IntroActivity.class);
            startActivity(intent);

            finish();
        }
    }
}
