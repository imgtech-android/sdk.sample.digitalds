package com.mimacstudy.player;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import java.net.URISyntaxException;

import kr.imgtech.lib.zoneplayer.IMGApplication;

/**
 * 다운로드 요청 정보 수신 Activity
 * Scheme 수신은 Activity 만 가능
 * @author kimsanghwan
 * @since 2015.09.17.
 */
public class DownloadRequestActivity extends AppCompatActivity {

    // 다운로드 요청 URL Key
    private final static String URL_DOWNLOAD_REQ = "url_download_req";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_download_request);

        finish();

        new DownloadRequestAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * 다운로드 요청 URL 저장
     * @param strUrl    다운로드 요청 URL
     */
    @SuppressLint("CommitPrefEdits")
    public static void setUrlDownloadReq(String strUrl) {

        SharedPreferences preferences
                = PreferenceManager.getDefaultSharedPreferences(IMGApplication.getContext());

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(URL_DOWNLOAD_REQ, strUrl);
        editor.commit();
    }

    /**
     * 다운로드 요청 URL 반환
     * @return  다운로드 요청 URL
     */
    public static String getUrlDownloadReq() {

        SharedPreferences preferences
                = PreferenceManager.getDefaultSharedPreferences(IMGApplication.getContext());

        return preferences.getString(URL_DOWNLOAD_REQ, "");
    }

    /**
     * 다운로드 처리 Activity 호출
     */
    private void startMainActivity() {

        Resources res = getResources();

        String strUri = "mimacstudy" + "://" +
                        res.getString(R.string.host_download_working);
        // 해당 Player 로 Intent 전달
        try {
            Intent i = Intent.parseUri(strUri, Intent.URI_INTENT_SCHEME);
            i.addCategory(Intent.CATEGORY_BROWSABLE);
            i.setPackage(getPackageName());
            startActivity(i);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * 다운로드 인텐트를 DB 저장 처리 AsyncTask
     */
    private class DownloadRequestAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            Intent intent = getIntent();
            setUrlDownloadReq(intent.getDataString());

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            startMainActivity();
        }
    }
}
