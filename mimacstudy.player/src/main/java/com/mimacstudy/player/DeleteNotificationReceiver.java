package com.mimacstudy.player;

import android.content.Context;
import android.content.Intent;

import kr.imgtech.lib.zoneplayer.gui.download.CancelNotificationReceiver;

/**
 * Notification Cancel BroadcastReceiver
 * @author kimsanghwan
 * @since  2014. 9. 11.
 */
public class DeleteNotificationReceiver extends CancelNotificationReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
	}

}
