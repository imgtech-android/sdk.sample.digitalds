package com.mimacstudy.player.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.mimacstudy.player.R;

import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;

/**
 * 일반 DialogFragment
 * @author kimsanghwan
 * @since 2016.04.05
 */
public class CommonDialog extends DialogFragment implements BaseInterface {

    // Tag
    public final static String DIALOG_TAG = "baseDialog";

    // 다이얼로그 입력 선택 인덱스
    public final static int DIALOG_CHOICE_POSITIVE = 1;			// 확인 등 실행의 선택
    //public final static int DIALOG_CHOICE_NEUTRAL = 0;		// 중립 선택
    public final static int DIALOG_CHOICE_NEGATIVE = -1;		// 아니오 등 거부의 선택
    public final static int DIALOG_CANCEL = 2;					// 버튼 선택없이 백버튼으로 취소

    // 다이얼로그 상태
    public final static int STATUS_AGREE_PUSH = 1000;           // Push 알림 동의
    public final static int STATUS_UPDATE_ALERT = 1001;         // 업데이트 알림
    public final static int STATUS_MESSAGE_ALERT = 1002;        // Push Message 알림
    public final static int STATUS_NETWORK_ERROR = 1003;        // 통신 오류 알림
    public final static int STATUS_INSTALL_VIDEO_EDITOR = 1004; // 동영상 편집기 설치 알림
    public final static int STATUS_START_VIDEO_EDITOR = 1005;   // 동영상 편집기 실행 알림
    public final static int STATUS_PERMISSION_NEED = 1006;      // 권한 필요 알림

    // 상태키
    private final static String KEY_STATE = "state";			// 다이얼로그 상태키
    private final static String KEY_CODE = "code";              // 다이얼로그 코드키
    private final static String KEY_TITLE = "title";            // 다이얼로그 타이틀키
    private final static String KEY_MESSAGE = "message";        // 다이얼로그 메시지키
    private final static String KEY_URI = "uri";

    /*
     * Member Variables
     */
    private int state;					// 다이얼로그 상태
    private int code;                   // 다이얼로그 코드
    private String title;               // 다이얼로그 타이틀
    private String message;             // 다이얼로그 메시지
    private Uri uri;                    // 다이얼로그 Uri
    private BaseDialogListener listener;// 다이얼로그 선택 이벤트 수신 리스너

    /**
     * 생성자
     */
    public CommonDialog() {
        super();
    }

    /**
     * 다이얼로그 생성 및 반환
     * @return CommonDialog
     */
    public static CommonDialog getInstance(int state) {

        CommonDialog dialog = new CommonDialog();

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);

        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성 및 반환
     * @param state 다이얼로그 상태
     * @param l 다이얼로그 리스너
     * @return  CommonDialog
     */
    public static CommonDialog getInstance(int state, BaseDialogListener l) {

        CommonDialog dialog = new CommonDialog();

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);

        dialog.listener = l;
        dialog.setArguments(args);

        return dialog;
    }

    /**
     * 다이얼로그 생성 및 반환
     * @param state 다이얼로그 상태
     * @param uri   Uri
     * @param l     다이얼로그 리스너
     * @return  CommonDialog
     */
    public static CommonDialog getInstance(int state, Uri uri, BaseDialogListener l) {

        CommonDialog dialog = new CommonDialog();

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);

        args.putParcelable(KEY_URI, uri);

        dialog.listener = l;
        dialog.setArguments(args);

        return dialog;
    }

    public static CommonDialog getInstance(int state, int code, String message, BaseDialogListener listener) {
        CommonDialog dialog = new CommonDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);
        args.putInt(KEY_CODE, code);
        args.putString(KEY_MESSAGE, message);

        dialog.setArguments(args);

        return dialog;
    }

    public static CommonDialog getInstance(int state, int code, String title, String message, BaseDialogListener listener) {
        CommonDialog dialog = new CommonDialog();

        dialog.listener = listener;

        Bundle args = new Bundle();
        args.putInt(KEY_STATE, state);
        args.putInt(KEY_CODE, code);
        args.putString(KEY_TITLE, title);
        args.putString(KEY_MESSAGE, message);

        dialog.setArguments(args);

        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle arguments = getArguments();

        // 상태값 초기화
        state = 0;
        code = 0;
        title = null;
        message = null;
        uri = null;

        // 상태값 설정
        state = arguments.getInt(KEY_STATE);

        if (arguments.containsKey(KEY_CODE)) {	// 코드가 존재하면
            code = arguments.getInt(KEY_CODE);
        }

        if (arguments.containsKey(KEY_TITLE)) {	// 메시지가 존재하면
            title = arguments.getString(KEY_TITLE);
        }

        if (arguments.containsKey(KEY_MESSAGE)) {	// 메시지가 존재하면
            message = arguments.getString(KEY_MESSAGE);
        }

        if (arguments.containsKey(KEY_URI)) {
            uri = arguments.getParcelable(KEY_URI);
        }

        switch (state) {

            case STATUS_PERMISSION_NEED:
                builder.setMessage(R.string.dialog_permission_need);
                builder.setPositiveButton("확인", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_POSITIVE);
                    }
                });
                builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        listener.onDialog(state, 0, DIALOG_CHOICE_NEGATIVE);
                    }
                });
                break;

        }

        return builder.create();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * 다이얼로그 표출시 백버튼 터치 이벤트
     * 백버튼 터치 시 이벤트 수신
     *
     */
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        switch (state) {

            default:
                listener.onDialog(state, 0, DIALOG_CANCEL);
                break;
        }
    }

}

