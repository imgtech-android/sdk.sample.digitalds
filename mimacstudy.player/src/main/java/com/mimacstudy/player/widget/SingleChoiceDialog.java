package com.mimacstudy.player.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.mimacstudy.player.R;
import com.mimacstudy.player.listener.SingleChoiceDialogListener;

/**
 * Single Choice Dialog 생성
 * @author kimsanghwan
 * @since  2014. 10. 10.
 */
public class SingleChoiceDialog extends DialogFragment {
	
	public final static String DIALOG_TAG = "ZONEPLAYER_SINGLE_CHOICE_DIALOG";

	public final static int PLAYER_MODE = 0;
	
	private final static String KEY_STATE = "state";		// 다이얼로그 상태키
	private final static String KEY_SELECTED = "selected";	// 선택 인덱스 키
	
	private int state;									// 다이얼로그 상태
	private int selected;								// 다이얼로그 선택 인덱스
	private SingleChoiceDialogListener listener;		// 다이얼로그 선택을 받는 리스너
	
	/**
	 * SingleChoiceDialog 생성
	 * @param state	다이얼로그 상태
	 * @return	SingleChoiceDialog
	 */
	public static SingleChoiceDialog getInstance(int state) {
		
		SingleChoiceDialog dialog = new SingleChoiceDialog();
		
		Bundle args = new Bundle();
		args.putInt(KEY_STATE, state);
		
		dialog.setArguments(args);
		
		return dialog;
	}
	
	/**
	 * SingleChoiceDialog 생성
	 * @param state	다이얼로그 상태
	 * @param l		SingleChoiceDialogListener
	 * @return	SingleChoiceDialog
	 */
	public static SingleChoiceDialog getInstance(int state, SingleChoiceDialogListener l) {
		
		SingleChoiceDialog dialog = new SingleChoiceDialog();
		dialog.listener = l;
		
		Bundle args = new Bundle();
		args.putInt(KEY_STATE, state);
		
		dialog.setArguments(args);
		
		return dialog;
	}
	
	/**
	 * SingleChoiceDialog 생성
	 * @param state	다이얼로그 상태
	 * @param selected 기본 선택 인덱스
	 * @param l		SingleChoiceDialogListener
	 * @return	SingleChoiceDialog
	 */
	public static SingleChoiceDialog getInstance(int state, int selected, SingleChoiceDialogListener l) {
		
		SingleChoiceDialog dialog = new SingleChoiceDialog();
		dialog.listener = l;
		
		Bundle args = new Bundle();
		args.putInt(KEY_STATE, state);
		args.putInt(KEY_SELECTED, selected);
		
		dialog.setArguments(args);
		
		return dialog;
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		Bundle arguments = getArguments();
		
		state = arguments.getInt(KEY_STATE);
		selected = arguments.getInt(KEY_SELECTED);
		
		switch (state) {

			case PLAYER_MODE:
				builder.setTitle(R.string.config_player_mode);
				builder.setSingleChoiceItems(R.array.config_player_mode_option, selected, selectItemListener);
				builder.setPositiveButton(R.string.cancel, dismissListener);
				break;
			
		}
		
		return builder.create();
	}

	/**
	 * SingleChoice 후 다이얼로그 종료 리스너
	 */
	OnClickListener dismissListener = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			dialog.dismiss();
		}
		
	};
	
	/**
	 * SingleChoice 후 선택 인덱스를 리스너에 전달하고
	 * 다이얼로그 종료 리스너
	 */
	OnClickListener selectItemListener = new OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			if (listener != null) {
				listener.onDialog(state, which);
			}
			dialog.dismiss();
		}
		
	};
}
