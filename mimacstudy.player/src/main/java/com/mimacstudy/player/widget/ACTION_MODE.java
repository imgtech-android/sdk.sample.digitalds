/**
 * 
 */
package com.mimacstudy.player.widget;

/**
 * ActionBar Mode 정의
 * @author kimsanghwan
 * @since 2014. 11. 17.
 */
public enum ACTION_MODE {

	NORMAL,
	DELETE,
	IDLE,
	SCROLL,
}
