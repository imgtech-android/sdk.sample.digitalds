package com.mimacstudy.player.listener;

/**
 * EditTextDialog Event Listener
 * @author kimsanghwan
 * @since  2014. 10. 20.
 */
public interface EditTextDialogListener {

	/**
	 * EditTextDialog 메시지 전달
	 *
	 * @param state	Dialog 상태
	 * @param input	Dialog 로부터 전달받은 입력 메시지
	 */
	public void onDialog(int state, String input);
}
