package com.mimacstudy.player.listener;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import java.util.ArrayList;

/**
 * Delete CheckBox CheckedChangeListener
 * @author kimsanghwan
 * @since  2014. 11. 19.
 */
public class CheckBoxChangeListener implements OnCheckedChangeListener {

	private ArrayList<Boolean> arrayList;	// CheckBox Check 여부 저장 ArrayList
	private int position;					// CheckBox position
	
	/**
	 * 생성자
	 *
	 * @param list	CheckBox Check 여부 저장 ArrayList
	 * @param p		CheckBox position
	 */
	public CheckBoxChangeListener(ArrayList<Boolean> list, int p) {
		
		arrayList = list;
		position = p;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		arrayList.set(position, isChecked);

	}
}
