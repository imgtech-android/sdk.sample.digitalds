package com.mimacstudy.player.listener;

import java.util.ArrayList;

/**
 * List 의 CheckBox 에서 checked 된 아이템 반환 리스너
 * @author kimsanghwan
 * @since  2014. 11. 26.
 */
public interface ICheckedListListener {

	/**
	 * checked 된 Item 을 ArrayList 형태로 일괄 반환
	 *
	 * @return	checked ArrayList
	 */
	ArrayList<Boolean> getCheckedList();
	
	/**
	 * Checked List 초기화
	 */
	void initCheckedList();
}
