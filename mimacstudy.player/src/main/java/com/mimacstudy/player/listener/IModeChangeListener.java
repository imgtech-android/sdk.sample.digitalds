package com.mimacstudy.player.listener;

import com.mimacstudy.player.widget.ACTION_MODE;

/**
 * ListView 모드 변경 Listener
 * @author kimsanghwan
 * @since  2014. 11. 18.
 */
public interface IModeChangeListener {

	/**
	 * ListView 모드 변경 실행
	 * @param mode	ACTION_MODE
	 */
	void changeMode(ACTION_MODE mode);
}
