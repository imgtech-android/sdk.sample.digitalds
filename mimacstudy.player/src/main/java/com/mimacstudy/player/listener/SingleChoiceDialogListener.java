package com.mimacstudy.player.listener;

/**
 * SingleChoiceDialog Event Listener
 * @author kimsanghwan
 * @since 2014. 10. 10.
 */
public interface SingleChoiceDialogListener {

	/**
	 * SingleChoiceDialog 선택 이벤트 전달
	 * @param state		Dialog 구분 상태
	 * @param selected	선택된 인덱스
	 */
	void onDialog(int state, int selected);
}
