package com.mimacstudy.player;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.mimacstudy.player.fragment.ContentsFragment;
import com.mimacstudy.player.fragment.CourseGroupFragment;
import com.mimacstudy.player.fragment.DownloadProgressFragment;
import com.mimacstudy.player.fragment.MainFragment;
import com.mimacstudy.player.fragment.SettingsFragment;
import com.mimacstudy.player.listener.IBackPressedListener;
import com.mimacstudy.player.listener.OnFragmentInteractionListener;

import java.net.URISyntaxException;

import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.LibUtil;
import kr.imgtech.lib.zoneplayer.util.StringUtil;

public class MainActivity extends AppCompatActivity implements BaseInterface, NavigationDrawerFragment.NavigationDrawerCallbacks, OnFragmentInteractionListener {

    public final static String KEY_INTENT = "intent";	// Fragment 간 데이터 Intent Key. use in all fragment
    public final static String MIMAC_SITE_ID = "71";   // 대성마이맥 Site ID. 고정

    /*
     * 화면 ID
     */
    public final static int ID_MAIN = 0;
    public final static int ID_COURSE_GROUP = 1;
    public final static int ID_CONTENTS = 11;
    public final static int ID_DOWNLOAD = 2;
    public final static int ID_SETTINGS = 3;

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private IBackPressedListener mBackPressedListener;
    private Toolbar mToolbar;                       // ActionBar ToolBar
    public ViewGroup mActionModeViewGroup;		    // Action Mode View Group
    private DownloadProgressFragment mDownloadProgressFragment; // DownloadProgressFragment

    // 종료 처리를 위한 상수
    private final int FINISH_OK = 0;
    private final int FINISH_TIME = 2000;
    private boolean isFinishOk;		// 종료 확인 여부

    // 종료 처리를 위한 핸들러
    private Handler finishHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == FINISH_OK) {
                isFinishOk = false;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConfigurationManager.setAllowCapture(this, true);

        // View Initiation
        mActionModeViewGroup = (ViewGroup) findViewById(R.id.action_mode);

        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        startIntentProcessing(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        startIntentProcessing(intent);
    }

    @Override
    public void onBackPressed() {

        if (mNavigationDrawerFragment.isDrawerOpen()) {
            mNavigationDrawerFragment.closeDrawer();
            return;
        }

        if (mBackPressedListener != null) {
            mBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ftx = fragmentManager.beginTransaction();

        // Back Pressed 수신할 Listener 있는 경우에 대한 초기화
        // Fragment -> IBackPressedListener Casting
        Fragment fragment;
        if (mBackPressedListener != null) {
            mBackPressedListener = null;
        }
        switch (position) {

            // 메인
            case ID_MAIN:

                fragment = MainFragment.newInstance();

                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;
                break;

            // 보관함
            case ID_COURSE_GROUP:

                Bundle dle = new Bundle();
                dle.putParcelable(MainActivity.KEY_INTENT, makeCourseGroupIntent(MIMAC_SITE_ID));

                fragment = CourseGroupFragment.newInstance(dle);

                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;
                break;

            // 다운로드
            case ID_DOWNLOAD:

                if (mDownloadProgressFragment != null) {
                    ftx.remove(mDownloadProgressFragment);
                    mDownloadProgressFragment = null;
                }

                mDownloadProgressFragment = DownloadProgressFragment.newInstance();
                ftx.replace(R.id.container, mDownloadProgressFragment, mDownloadProgressFragment.getClass().getName())
                        .addToBackStack(null);
                mBackPressedListener = mDownloadProgressFragment;
                break;

            // 환경설정
            case ID_SETTINGS:

                fragment = SettingsFragment.newInstance();

                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;
                break;

            default:

                break;
        }

        ftx.commitAllowingStateLoss();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * 종료 처리 핸들러 시작
     */
    private void startFinishHandler() {
        finishHandler.sendEmptyMessageDelayed(FINISH_OK, FINISH_TIME);
    }

    /**
     * Application 종료 일괄 처리
     */
    public void finishApplicationProcess() {

        if (isFinishOk) {

            finish();
        } else {

            isFinishOk = true;

            startFinishHandler();
            LibUtil.toaster(getApplicationContext(), R.string.toast_finish_confirm);
        }
    }


    /**
     * 전달된 Intent 처리
     * @param intent    전달된 Intent
     */
    private void startIntentProcessing(Intent intent) {

        if ((intent != null) && (intent.getData() != null)) {

            // 다운로드 Scheme
            if (StringUtil.equals(intent.getData().getHost(), getResources().getString(R.string.host_download_working))) {

                String urlDownloadReq = DownloadRequestActivity.getUrlDownloadReq();

                // 전달된 인텐트 있음
                if (StringUtil.isNotBlank(urlDownloadReq)) {

                    // 다운로드 요청 URL -> Intent 변환
                    Intent downloadIntent = null;
                    try {
                        downloadIntent = Intent.parseUri(urlDownloadReq, Intent.URI_INTENT_SCHEME);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                    // 번들 구성
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(DownloadProgressFragment.KEY_INTENT, downloadIntent);

                    // 저장된 다운로드 요청 URL 삭제
                    DownloadRequestActivity.setUrlDownloadReq("");

                    switchFragment(ID_DOWNLOAD, bundle);
                } else {

                    switchFragment(ID_DOWNLOAD, null);
                }
            }

        } else {

            switchFragment(ID_MAIN, null);
        }
    }

    /**
     * Toolbar 화면 표출 설정
     * @param view	true: 표출, false: otherwise
     */
    private void setToolbarVisible(boolean view) {

        if (view) {
            mToolbar.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * ActionMode 설정
     * @param view	설정할 ActionMode View
     */
    public void setActionModeView(View view) {

        if (view != null) {

            setToolbarVisible(false);
            mActionModeViewGroup.setVisibility(View.VISIBLE);
            mActionModeViewGroup.addView(view);
        }
    }

    /**
     * ActionMode 해제
     * @param view	해제할 ActionMode View
     */
    public void releaseActionModeView(View view) {

        if (view != null) {

            setToolbarVisible(true);
            mActionModeViewGroup.removeView(view);
            mActionModeViewGroup.setVisibility(View.GONE);
        }
    }

    /**
     * Fragment 변경 Replace
     * @param id		변경할 화면 ID
     * @param bundle	Fragment 에 전달할 Intent Bundle
     */
    public void switchFragment(int id, Bundle bundle) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ftx = fragmentManager.beginTransaction();

        // Back Pressed 수신할 Listener 있는 경우에 대한 초기화
        // Fragment -> IBackPressedListener Casting
        Fragment fragment;
        if (mBackPressedListener != null) {
            mBackPressedListener = null;
        }

        switch (id) {

            // 메인
            case ID_MAIN:

                fragment = MainFragment.newInstance();

                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;
                break;

            // 보관함 - 강좌 그룹 화면
            case ID_COURSE_GROUP :

                Bundle dle = new Bundle();
                dle.putParcelable(MainActivity.KEY_INTENT, makeCourseGroupIntent(MIMAC_SITE_ID));

                fragment = CourseGroupFragment.newInstance(dle);

                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;
                break;

            // 보관함 - 콘텐츠 목록 화면
            case ID_CONTENTS:
                if (bundle != null) {
                    fragment = ContentsFragment.newInstance(bundle);
                } else {
                    fragment = ContentsFragment.newInstance();
                }

                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;
                break;

            // 다운로드 화면
            case ID_DOWNLOAD :
                if (mDownloadProgressFragment != null) {
                    ftx.remove(mDownloadProgressFragment);
                    mDownloadProgressFragment = null;
                }

                if (bundle != null) {
                    mDownloadProgressFragment = DownloadProgressFragment.newInstance(bundle);
                } else {
                    mDownloadProgressFragment = DownloadProgressFragment.newInstance();
                }

                ftx.replace(R.id.container, mDownloadProgressFragment, mDownloadProgressFragment.getClass().getName())
                        .addToBackStack(null);
                mBackPressedListener = mDownloadProgressFragment;
                break;

            // 환경설정
            case ID_SETTINGS :
                fragment = SettingsFragment.newInstance();

                ftx.replace(R.id.container, fragment);
                mBackPressedListener = (IBackPressedListener) fragment;
                break;

            default :

                break;

        }

        ftx.commitAllowingStateLoss();
    }

    /**
     * Course Group Fragment Intent 생성
     * @param siteId	Site ID
     * @return	Course Group Fragment Intent
     */
    private Intent makeCourseGroupIntent(String siteId) {

        Intent intent = null;

        String scheme = getResources().getString(R.string.scheme_player);

        String uri = String.format((scheme + "://courselist?"
                        + "siteid=%s"
                        + "&courseid=%s"),
                siteId,
                "test")
                ;

        // Intent 설정
        try {
            intent = Intent.parseUri(uri, Intent.URI_INTENT_SCHEME);
            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getPackageName());
            intent.setPackage(getPackageName());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return intent;
    }
}
