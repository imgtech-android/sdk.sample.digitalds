package com.mimacstudy.player;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mimacstudy.player.widget.CommonDialog;

import java.util.ArrayList;
import java.util.List;

import kr.imgtech.lib.zoneplayer.data.BaseInterface;
import kr.imgtech.lib.zoneplayer.interfaces.BaseDialogListener;
import kr.imgtech.lib.zoneplayer.util.ConfigurationManager;
import kr.imgtech.lib.zoneplayer.util.LibUtil;

/**
 * 인트로 화면
 */
public class IntroActivity extends AppCompatActivity implements BaseInterface, BaseDialogListener {

    // 저장소 저장 권한 코드
    private final int MY_PERMISSIONS_REQUEST = 1;

    @SuppressLint("InlinedApi")
    private final String[] ARRAY_PERMISSION = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private CommonDialog mDialog;

    private List<Runnable> queue = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        removeControlBar();

        if (SettingsManager.getAllowPermission(this)) {
            new IntroAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            startRequestPermission();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        for (Runnable work : queue) {

            (new Thread(work)).start();
        }
        queue.clear();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST:

                // 전체 권한 허용 여부 확인
                boolean grantAll = true;
                for (int result : grantResults) {

                    if (result != PackageManager.PERMISSION_GRANTED) {
                        grantAll = false;
                        break;
                    }
                }

                if (grantAll) {

                    startMainActivity();

                } else {

                    queue.add(new Runnable() {

                        @Override
                        public void run() {
                            dialogPermissionNeed();
                        }
                    });
                }

                break;
        }
    }

    @Override
    public void onDialog(int state, int code, int result) {

        switch (state) {

            case CommonDialog.STATUS_PERMISSION_NEED:

                if (result == CommonDialog.DIALOG_CHOICE_POSITIVE) {
                    checkPermission();
                } else {
                    finish();
                }

                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void removeControlBar(){

        int visibility = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            visibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            visibility |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(visibility);
    }

    /**
     * 메인 화면 전환
     */
    private void startMainActivity() {

        finish();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * 권한 확인 처리
     */
    private void checkPermission() {

        if (LibUtil.isMashMallowOrLater()) {

            // 전체 권한 허용 여부 확인
            boolean grantAll = true;
            for (String permission : ARRAY_PERMISSION) {

                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    grantAll = false;
                    break;
                }
            }

            // 저장소 사용 권한
            if (grantAll) {

                startMainActivity();

            } else {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(this, ARRAY_PERMISSION,
                            MY_PERMISSIONS_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, ARRAY_PERMISSION,
                            MY_PERMISSIONS_REQUEST);
                }
            }

        } else {

            startMainActivity();
        }
    }

    /**
     * 권한 허용 화면 표시
     */
    public void startRequestPermission() {

        Intent intent = new Intent(this, RequestPermissionActivity.class);
        startActivity(intent);

        finish();
    }

    /**
     * 권한 필요 알림
     */
    private void dialogPermissionNeed() {

        mDialog = CommonDialog.getInstance(CommonDialog.STATUS_PERMISSION_NEED, this);
        mDialog.show(getSupportFragmentManager(), CommonDialog.DIALOG_TAG);
    }

    /**
     * 인트로 화면 일괄 처리
     */
    private class IntroAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // M 버전은 절대 통합 플레이어로 설정
            if (LibUtil.isMashMallowOrLater()) {
                ConfigurationManager.setPlayerMode(getApplicationContext(), 3);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //startMainActivity();
            checkPermission();
        }
    }

}
